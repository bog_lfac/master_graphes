/*
 *   Programme : Bibliotheque de manipulation de graphes.
 *   
 *   Ecrit par : BOG et Aurélie Sellier
 *   
 *   Resume :    Bibliotheque permettant la creation, la lecture, la modification
 *               ainsi que la suppression de graphes.
 *               
 *   Date :      18/10/16
 */
 
#ifndef LIBGRAPHE_H
#define LIBGRAPHE_H
// Entetes systeme
#include <stdlib.h>
// Entetes projet
#include <erreur.h>
#define GRAPHE_TAILLE_MAX 256
#include <liste.h>

typedef struct TypGraphe TypGraphe;
typedef struct tas_arete_t tas_arete_t;

enum
  {
    GRAPHE_OK 
    , GRAPHE_ERREUR
  };

enum
  {
    GRAPHE_ORIENTE = 1
    , GRAPHE_NON_ORIENTE = 0
  };

struct TypGraphe
{
  int estOriente;               /* Permet de savoir si le graphe 
				   est oriente */
                                                        
  size_t nbMaxSommets;          /* Nombre maximum de sommet pouvant 
				   appartenir au graphe. */
  
  size_t nbSommets;            /* Nombre effectif de sommet 
				  appartenant au graphe. */
  
  size_t nbAretes;             /* Nombre effectif d'aretes
				  appartenant au graphe. */
  
  TypVoisins** listesAdjacences; /* Listes d'adjacence du graphe. */
  /* Tableau de pointeurs vers des listes. */
};

struct tas_arete_t
{
  int u;
  int v;
  double poids;
};

erreur_t creationTypGraphe(TypGraphe* graphe
			   , int estOriente
			   , size_t nbMaxSommets);

int comparerTasArete(void* a1, void* a2);

erreur_t creationArpmTypGraphe(TypGraphe* graphe, TypGraphe* arpm);

erreur_t suppressionTypGraphe(TypGraphe* graphe);

TypVoisins* sommetTypGraphe(TypGraphe* graphe, size_t sommet);

int valeurSommetTypGraphe(TypGraphe* graphe, size_t sommet);
  
erreur_t insertionSommetTypGraphe(TypGraphe* graphe, size_t sommet, int valeur);

TypVoisins* areteTypGraphe(TypGraphe* graphe
			   , size_t predecesseur
			   , size_t successeur);

erreur_t insertionAreteTypGraphe(TypGraphe* graphe
                                 , size_t predecesseur
                                 , size_t successeur
				 , double poids);

erreur_t insertionAreteSymetriqueTypGraphe(TypGraphe* graphe
                                      	   , size_t sommet1
                                      	   , size_t sommet2
					   , double poids);

erreur_t suppressionSommetTypGraphe(TypGraphe* graphe, size_t sommet);

erreur_t suppressionAreteTypGraphe(TypGraphe* graphe
                                   , size_t predecesseur
                                   , size_t successeur);
                                   
erreur_t suppressionAreteSymetriqueTypGraphe(TypGraphe* graphe
					     , size_t sommet1
					     , size_t sommet2);

erreur_t representationTypGraphe(TypGraphe* graphe, FILE* sortie);
                                
erreur_t affichageTypGraphe(TypGraphe* graphe);

#endif
