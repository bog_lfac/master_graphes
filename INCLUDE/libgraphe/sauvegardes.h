/*
 *   Programme : Bibliotheque de sauvegarde de graphes.
 *   
 *   Ecrit par : BOG et Aurélie Sellier
 *   
 *   Resume :    Bibliotheque permettant la sauvegarde et le 
 *               chargement de graphes.
 *               
 *   Date :      31/10/16
 */
 
#ifndef SAUVEGARDES_H
#define SAUVEGARDES_H
// Entetes projet
#include <stdlib.h>
#include <libgraphe.h>

erreur_t sauvegardeTypGraphe(TypGraphe* graphe, char const* nomFichier); 

erreur_t chargementTypGraphe(TypGraphe* graphe, char const* nomFichier);

size_t lireLigne(char* ligne, size_t tailleMax, FILE* flux);

int lireNbMaxSommets(char* ligne, size_t tailleMax, FILE* flux);

int lireOrientation(char* ligne, size_t tailleMax, FILE* flux);

erreur_t chargementSommetsTypGraphe(TypGraphe* graphe
                                   , char* ligne
                                   , size_t tailleMax
                                   , FILE* flux);

erreur_t chargementAretes(TypGraphe* graphe
                          , char* ligne
                          , size_t tailleMax
                          , size_t sommet);

int identificationNombre(char* ligne, size_t indice, size_t tailleMax);

#endif
