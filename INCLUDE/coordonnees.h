/*
 *   Programme :  Calcul des coordonnees.
 *   		  
 *   Ecrit par :  BOG et Aurélie Sellier
 *   		  
 *   Resume    :  Stockage de coordonnees entieres dans un entier (non
 *                signe) sur 16 bits dans un entier (signe) de 32 bits.
 *                
 *   Date      :  5/10/16
 */

// Entetes systeme
#include <stdio.h>

void affichageCoordonneeFormatBinaire(int nb);
unsigned int coordonnee2D(unsigned short int x, unsigned short int y);
unsigned short int coordonneeX(unsigned int coordonnee);
unsigned short int coordonneeY(unsigned int coordonnee);
float distance2D(unsigned int coordonnee1, unsigned int coordonnee2);
