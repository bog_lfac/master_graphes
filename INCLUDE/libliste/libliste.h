/*
 *   Programme : Bibliotheque de manipulation de listes.
 *   
 *   Ecrit par : BOG et Aurélie Sellier
 *   
 *   Resume :    Bibliotheque permettant la creation, la lecture, la modification
 *               ainsi que la suppression de listes circulaires doublement 
 *               chainees avec sentinelles.
 *               
 *   Date :      5/10/16
 */
#ifndef LIBLISTE_H
#define LIBLISTE_H
#define VALEUR_SENTINELLE (-1)
// Entetes projet
#include <erreur.h>

typedef struct TypVoisins TypVoisins;

enum
{
    VOISIN_OK
    , VOISIN_ERREUR
};

struct TypVoisins
{
	int    voisin;                         /* valeur de l'element de la liste */
	double poids;                          /* poids de l'element de la liste */
	struct TypVoisins* voisinSuivant;      /* element suivant dans la liste */
        struct TypVoisins* voisinPrecedent;    /* element precedent dans la liste */
};

/* Prototype des fonctions */

erreur_t creationTypVoisins(TypVoisins* voisin, int valeur, double poids);

erreur_t suppressionTypVoisins(TypVoisins* voisin);

int valeurTypVoisins(TypVoisins* voisin);

double poidsTypVoisins(TypVoisins* voisin);

TypVoisins* creationQueueTypVoisins(TypVoisins* voisin, int valeur, double poids);

erreur_t insertionQueueTypVoisins(TypVoisins* voisin, TypVoisins* nouveau);

erreur_t suppressionQueueTypVoisins(TypVoisins* voisin);

erreur_t suppressionVoisinTypVoisins(TypVoisins* voisin);

TypVoisins* rechercheValeurTypVoisins(TypVoisins* voisin, int valeur);

TypVoisins* suivantTypVoisins(TypVoisins* voisin);

TypVoisins* precedentTypVoisins(TypVoisins* voisin);

TypVoisins* premierTypVoisins(TypVoisins* voisin);

TypVoisins* dernierTypVoisins(TypVoisins* voisin);

erreur_t representationTypVoisins(TypVoisins* voisin, FILE* sortie);

erreur_t affichageTypVoisins(TypVoisins* voisin);

#endif
