/*
 *   Programme : Bibliotheque de manipulation de file de priorite implementee 
 *               par des tas.
 *   
 *   Ecrit par : BOG et Aurélie Sellier
 *   
 *   Resume :    Bibliotheque permettant la creation, l'ajout, le retrait
 *               ainsi que la suppression de tas.
 *               
 *   Date :      30/11/16
 */
#ifndef LIBTAS_H
#define LIBTAS_H

typedef int (*compare_fct) (void*, void*);

size_t tas_pere(size_t indice);
size_t tas_fils_gauche(size_t indice);
size_t tas_fils_droit(size_t indice);

void tas_entassement(void** tableau, size_t taille, compare_fct comparer, size_t indice);

void tas_construction(void** tableau, size_t taille, compare_fct comparer);

void* tas_extraction(void** tableau, size_t* taille, compare_fct comparer);

void tas_ajout(void** tableau, size_t* taille, compare_fct comparer, void* element);

#endif 
