/*
 *   Programme : Bibliotheque d'interface.
 *   
 *   Ecrit par : BOG et Aurélie Sellier
 *   
 *   Resume :    Bibliotheque permettant d'interfacer les fonctions de 
 *               manipulation de graphes.
 *               
 *   Date :      02/11/16
 */
#ifndef GRAPHE_CLI_H
#define GRAPHE_CLI_H
// Entetes systeme
// Entetes projet
#include <graphe.h>
#include <erreur.h>

typedef struct graphe_cli_t graphe_cli_t;

struct graphe_cli_t
{
	TypGraphe* graphe;
	int grapheInitialise;
};

void entreeInterface(graphe_cli_t* interface);

void sortieInterface(graphe_cli_t* interface);

void menuPreCreation(graphe_cli_t* interface);

void creationGrapheInterface(graphe_cli_t* interface);

void chargementGrapheInterface(graphe_cli_t* interface);

void menuPostCreation(graphe_cli_t* interface);

void insertionSommetGrapheInterface(graphe_cli_t* interface);

void suppressionSommetGrapheInterface(graphe_cli_t* interface);

void insertionAreteGrapheInterface(graphe_cli_t* interface);

void suppressionAreteGrapheInterface(graphe_cli_t* interface);

void sauvegardeGrapheInterface(graphe_cli_t* interface);

void saisieChaine(char* chaine, size_t taille);

double saisieNombre();

void messageSucces(char* chaine);

void messageEchec(char* chaine);

#endif
