#ifndef PVC_H
#define PVC_H
#include <stdio.h>
#include <graphe.h>

typedef double (*solution_fct) (TypGraphe* graphe, int* cycle);
typedef struct sommet_t sommet_t;

struct sommet_t
{
  int numero;
  TypGraphe* graphe;
};

int factorielle (int n);

void exportationParcours(TypGraphe* graphe, int* cycle, char* fichier_nom, char* titre);
  
// Solution optimale
double pvc_optimal (TypGraphe* graphe, int* cycle);
void pvc_optimal_partiel (TypGraphe* graphe, int* cycleCourant, int tailleCycle, double coutCourant, int* meilleurCycle, double* meilleurCout);

// Plus proche voisin
double pvc_plus_proche_voisin (TypGraphe* graphe, int* cycle);

// Plus petit detour
double pvc_plus_petit_detour (TypGraphe* graphe, int* cycle);

// Basé sur les ARPM
double pvc_arpm (TypGraphe* graphe, int* cycle);

void pvc_ajout_sommet_cycle (TypGraphe* graphe, int* cycle, int sommet, int predecesseur);
void pvc_retrait_sommet_cycle (TypGraphe* graphe, int* cycle, int sommet);
double pvc_cout_cycle (TypGraphe* graphe, int* cycle);

#endif
