/*
 *   Programme : Bibliotheque de gestion des erreurs.
 *   
 *   Ecrit par : BOG et Aurélie Sellier
 *   
 *   Resume :    Bibliotheque permettant la gestions des erreurs sous la forme
 *               d'une pile d'erreur.
 *               
 *   Date :      01/11/16
 */
#ifndef ERREUR_H
#define ERREUR_H
#define NB_MAX_ERREURS 128
// Entetes systeme
// Entetes projet

typedef int erreur_t;
typedef struct pile_erreur_t pile_erreur_t;

struct pile_erreur_t
{
	size_t taille;
	char* messages[NB_MAX_ERREURS];
};

pile_erreur_t* ERREUR;

void creationErreur(pile_erreur_t* erreur);
void suppressionErreur(pile_erreur_t* erreur);

void empilementErreur(pile_erreur_t* erreur, char* message);
void effacementErreur(pile_erreur_t* erreur);
void representationErreur(pile_erreur_t* erreur, FILE* flux);
void affichageErreur(pile_erreur_t* erreur);
#endif
