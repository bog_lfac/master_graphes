/*
 *   Programme : Bibliotheque de manipulation des ensembles disjoints.
 *   
 *   Ecrit par : BOG et Aurélie Sellier
 *   
 *   Resume    : Bibliotheque permettant la creation, l'union et l'acces
 *               au representant d'ensembles disjoints.
 *               
 *   Date :      15/12/16
 */

#ifndef ENSEMBLES_H
#define ENSEMBLES_H

typedef struct ensemble_t ensemble_t;

struct ensemble_t
{
  ensemble_t* representant;
  void* valeur;
  int rang;
};

void creationEnsemble(ensemble_t* ensemble, void* valeur);
void unionEnsemble(ensemble_t* ensemble, ensemble_t* autreEnsemble);
ensemble_t* representantEnsemble(ensemble_t* ensemble);

#endif
