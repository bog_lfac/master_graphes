// Entetes systeme
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
// Entetes projet
#include <graphe_cli.h>
#include <libliste.h>
#include <libgraphe.h>

extern pile_erreur_t* ERREUR;

/*
 *   Fonction :    entreeInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Allocation des ressources et entree dans l'interface.
 */
void entreeInterface(graphe_cli_t* interface)
{
  assert(interface);
	
  interface->graphe = (TypGraphe*) malloc( sizeof(TypGraphe) );
  interface->grapheInitialise = 0;
	
  ERREUR = (pile_erreur_t*) malloc( sizeof(pile_erreur_t) );
  creationErreur(ERREUR);
	
  menuPreCreation(interface);
}

/*
 *   Fonction :    sortieInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Liberation des ressources et sortie de l'interface.
 */
void sortieInterface(graphe_cli_t* interface)
{
  assert(interface);
	
  if (interface->grapheInitialise)
    {
      suppressionTypGraphe(interface->graphe);
    }
	
  free(interface->graphe);
	
  suppressionErreur(ERREUR);
  free(ERREUR);
  ERREUR = NULL;
}

/*
 *   Fonction :    menuPreCreation
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Menu disponible avant la creation du graphe.
 */
void menuPreCreation(graphe_cli_t* interface)
{
  assert(interface);

  printf("\n\n\tMenu\n");
  printf("1- Creer un nouveau graphe.\n");
  printf("2- Charger un graphe.\n");
  printf("3- Quitter\n");
	
  int reponse;
	
  do
    {
      reponse = saisieNombre();
    }
  while (reponse < 1 || reponse > 3);
	
  switch (reponse)
    {
    case 1 : creationGrapheInterface(interface); break;
		
    case 2 : chargementGrapheInterface(interface); break;
		
    case 3 : sortieInterface(interface); break;
    }
}

/*
 *   Fonction :    creationGrapheInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Creation d'un graphe.
 */
void creationGrapheInterface(graphe_cli_t* interface)
{
  assert(interface);
	
  // 1- Recuperation des informations utiles a la creation du graphe.
  printf("Veuillez saisir le nombre maximal de sommet\n");
	
  int nbMaxSommets = saisieNombre();
	
  printf("Veuillez saisir l'orientation du graphe (o - oriente, n - non oriente)\n");
	
  size_t const taille = 2;
  char orientationChaine[taille];
  saisieChaine((char*) &orientationChaine, taille);
	
  int orientation;
	
  if (strcmp(orientationChaine, "o") == 0)
    {
      orientation = GRAPHE_ORIENTE;
    }
  else if (strcmp(orientationChaine, "n") == 0)
    {
      orientation = GRAPHE_NON_ORIENTE;
    }
  else
    {
      // L'orientation saisie n'est pas exploitable par le programme.
      messageEchec("L'orientation saisie est différente de \"o\" et \"n\"");
      menuPreCreation(interface);
      return;
    }
	
  // 2- Creation du graphe.
  erreur_t err = creationTypGraphe(interface->graphe
				   , orientation
				   , nbMaxSommets);
  if (err == GRAPHE_OK)
    // 3- La creation s'est bien passee et on avance au menu post-creation.
    {
      interface->grapheInitialise = 1;
      messageSucces("creation du graphe");
      menuPostCreation(interface);
    }
  else
    // 3- La creation a echouee et on retourne au menu pre-creation.
    {
      messageEchec("creation du graphe");
      menuPreCreation(interface);
    }                                 
}

/*
 *   Fonction :    chargementGrapheInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Chargement d'un graphe.
 */
void chargementGrapheInterface(graphe_cli_t* interface)
{
  printf("Veuillez saisir le nom du fichier.\n");
  size_t const taille = 128;
  char nom[taille];
  saisieChaine(nom, taille);
	
  size_t const taille_message = 256;
  char message[taille_message];
  snprintf(message, taille_message, "chargement du fichier %s", nom);
	
  // 1- Chargement du graphe dans le fichier saisi.
  erreur_t err = chargementTypGraphe(interface->graphe, nom);
	
  if (err == GRAPHE_ERREUR)
    {
      // 2- Le chargement a echoue et on revient au menu pre-creation.
      messageEchec(message);
      menuPreCreation(interface);
    }
  else
    {
      // 2- Le chargement a reussi et on avance au menu post-creation.
      interface->grapheInitialise = 1;
      messageSucces(message);
      menuPostCreation(interface);
    }	
}

/*
 *   Fonction :    menuPostCreation
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Menu disponible apres la creation du graphe.
 */
void menuPostCreation(graphe_cli_t* interface)
{	
  assert(interface);

  printf("\n\n\tMenu\n");
  printf("1- Inserer un nouveau sommet\n");
  printf("2- Supprimer un sommet\n");
  printf("3- Inserer une nouvelle arete\n");
  printf("4- Supprimer une arete\n");
  printf("5- Afficher le graphe\n");
  printf("6- Sauvegarder le graphe\n");
  printf("7- Quitter\n");
	
  int reponse;
	
  do
    {
      reponse = saisieNombre();
    }
  while (reponse < 1 || reponse > 7);
	
  switch (reponse)
    {
    case 1 : insertionSommetGrapheInterface(interface); break;
    case 2 : suppressionSommetGrapheInterface(interface); break;
    case 3 : insertionAreteGrapheInterface(interface); break;
    case 4 : suppressionAreteGrapheInterface(interface); break; 
    case 5 : 
      printf("\e[33m");
      affichageTypGraphe(interface->graphe);
      printf("\e[0m");
      menuPostCreation(interface);
      break;
    case 6 : sauvegardeGrapheInterface(interface); break;
    case 7 : sortieInterface(interface); break;
    }
}

/*
 *   Fonction :    insertionSommetGrapheInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Insertion d'un sommet dans le graphe de l'interface.
 */
void insertionSommetGrapheInterface(graphe_cli_t* interface)
{
  assert(interface);
	
  printf("Veuillez saisir le numero du sommet a inserer.\n");
	
  int sommet = saisieNombre();
	
  erreur_t err = insertionSommetTypGraphe(interface->graphe, sommet, sommet);
	
  size_t const taille = 64;
  char chaine[taille];
  snprintf(chaine, taille, "insertion du sommet %d", sommet);
	
  if (err == GRAPHE_OK)
    {
      messageSucces(chaine);
    }
  else
    {
      messageEchec(chaine);
    }
	
  menuPostCreation(interface);
}

/*
 *   Fonction :    suppressionSommetGrapheInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Suppression d'un sommet dans le graphe de l'interface.
 */
void suppressionSommetGrapheInterface(graphe_cli_t* interface)
{
  assert(interface);
	
  printf("Veuillez saisir le numero du sommet a supprimer.\n");
	
  int sommet = saisieNombre();
	
  erreur_t err = suppressionSommetTypGraphe(interface->graphe, sommet);
	
  size_t const taille = 64;
  char chaine[taille];
  snprintf(chaine, taille, "suppression du sommet %d", sommet);
	
  if (err == GRAPHE_OK)
    {
      messageSucces(chaine);
    }
  else
    {
      messageEchec(chaine);
    }
	
  menuPostCreation(interface);
}

/*
 *   Fonction :    insertionAreteGrapheInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Insertion d'une arete dans le graphe de l'interface.
 */
void insertionAreteGrapheInterface(graphe_cli_t* interface)
{
  assert(interface);
	
  printf("Veuillez saisir le numero du sommet predecesseur.\n");
  int sommet1 = saisieNombre();
	
  printf("Veuillez saisir le numero du sommet successeur.\n");
  int sommet2 = saisieNombre();
	
  printf("Veuillez saisir le poids de l'arete.\n");
  double poids = saisieNombre();
	
  erreur_t err;
	
  size_t const taille = 64;
  char chaine[taille];
	
  if (interface->graphe->estOriente == GRAPHE_ORIENTE)
    {
      err = insertionAreteTypGraphe(interface->graphe, sommet1, sommet2, poids);
      snprintf(chaine
	       , taille
	       , "insertion de l'arete (%d)--%.2f-->(%d)"
	       , sommet1
	       , poids
	       , sommet2);
    }
  else
    {
      err = insertionAreteSymetriqueTypGraphe(interface->graphe
					      , sommet1
					      , sommet2
					      , poids);
      snprintf(chaine
	       , taille
	       , "insertion de l'arete symetrique (%d)--%.2f--(%d)"
	       , sommet1
	       , poids
	       , sommet2);
    }
	
	
  if (err == GRAPHE_OK)
    {
      messageSucces(chaine);
    }
  else
    {
      messageEchec(chaine);
    }
	
  menuPostCreation(interface);
}

/*
 *   Fonction :    suppressionAreteGrapheInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Suppression d'une arete dans le graphe de l'interface.
 */
void suppressionAreteGrapheInterface(graphe_cli_t* interface)
{
  assert(interface);
	
  printf("Veuillez saisir le numero du sommet predecesseur.\n");
  int sommet1 = saisieNombre();
	
  printf("Veuillez saisir le numero du sommet successeur.\n");
  int sommet2 = saisieNombre();

  erreur_t err;
	
  size_t const taille = 64;
  char chaine[taille];
	
  if (interface->graphe->estOriente == GRAPHE_ORIENTE)
    {
      err = suppressionAreteTypGraphe(interface->graphe, sommet1, sommet2);
      snprintf(chaine
	       , taille
	       , "suppression de l'arete (%d)---->(%d)"
	       , sommet1
	       , sommet2);
    }
  else
    {
      err = suppressionAreteSymetriqueTypGraphe(interface->graphe
						, sommet1
						, sommet2);
      snprintf(chaine
	       , taille
	       , "insertion de l'arete symetrique (%d)----(%d)"
	       , sommet1
	       , sommet2);
    }
	
	
  if (err == GRAPHE_OK)
    {
      messageSucces(chaine);
    }
  else
    {
      messageEchec(chaine);
    }
	
  menuPostCreation(interface);
}

/*
 *   Fonction :    sauvegardeGrapheInterface
 *   
 *   Parametres :  graphe_cli_t* interface, les donnees de l'interface.
 *   
 *   Retour :
 *                 
 *   Description : Sauvegarde d'un graphe.
 */
void sauvegardeGrapheInterface(graphe_cli_t* interface)
{
  printf("Veuillez saisir le nom du fichier.\n");
  size_t const taille = 128;
  char nom[taille];
  saisieChaine(nom, taille);
	
  // 1- Chargement du graphe dans le fichier saisi.
  erreur_t err = sauvegardeTypGraphe(interface->graphe, nom);
	
  size_t const taille_message = 256;
  char message[taille_message];
	
  snprintf(message, taille_message, "sauvegarde dans le fichier %s", nom);
	
  if (err == GRAPHE_OK)
    {
      messageSucces(message);
    }
  else
    {
      messageEchec(message);
    }	
	
  menuPostCreation(interface);
}

/*
 *   Fonction :    saisieChaine
 *   
 *   Parametres :  char* chaine, la chaine destination dans laquelle sera 
 *                               stockee la chaine saisie.
 *                 size_t taille, la taille de la chaine destination.
 *   
 *   Retour :
 *                 
 *   Description : Permet d'obtenir une chaine saisie par l'utilisateur.
 */
void saisieChaine(char* chaine, size_t taille)
{
  char caractere_lu;
  size_t lu;
  size_t indiceChaine = 0;
	
  do
    {
      lu = fread(&caractere_lu, sizeof(char), 1, stdin);
		
      chaine[indiceChaine] = caractere_lu;
      indiceChaine++;
		
    }
  while (lu > 0 
	 && caractere_lu != '\n'
	 && indiceChaine < taille);
	
  chaine[indiceChaine - 1] = '\0';
}

/*
 *   Fonction :    saisieNombre
 *   
 *   Parametres :  
 *   
 *   Retour :      double, le nombre saisi par l'utilisateur.
 *                 
 *   Description : Permet d'obtenir un nombre saisi par l'utilisateur
 */
double saisieNombre()
{
  size_t const taille = 64;
  char chaine[taille];
	
  saisieChaine(chaine, taille);

  return atof(chaine);
}

/*
 *   Fonction :    messageSucces
 *   
 *   Parametres :  char* chaine, le message de succes a afficher.
 *   
 *   Retour :
 *                 
 *   Description : Permet d'adresser un message de succes a l'utilisateur.
 */
void messageSucces(char* chaine)
{
  printf("\e[32mSUCCES : %s.\e[0m\n", chaine);
}

/*
 *   Fonction :    messageEchec
 *   
 *   Parametres :  char* chaine, le message d'echec a afficher.
 *   
 *   Retour :
 *                 
 *   Description : Permet d'adresser un message d'echec a l'utilisateur.
 */
void messageEchec(char* chaine)
{
  fprintf(stderr, "\e[31mECHEC : %s.\e[0m\n", chaine);
  affichageErreur(ERREUR);
}

