// Entetes systeme
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
// Entetes projet
#include <erreur.h>

extern pile_erreur_t* ERREUR;

/*
 *   Fonction :    creationErreur
 *   
 *   Parametres :  pile_erreur_t* erreur, l'erreur a creer.
 *   
 *   Retour :    
 *                 
 *   Description : Creation d'une pile d'erreur.
 */   
void creationErreur(pile_erreur_t* erreur)
{
  erreur->taille = 0;
}

/*
 *   Fonction :    suppressionErreur
 *   
 *   Parametres :  pile_erreur_t* erreur, l'erreur a supprimer.
 *   
 *   Retour :    
 *                 
 *   Description : Suppression d'une pile d'erreur.
 */   
void suppressionErreur(pile_erreur_t* erreur)
{
  for(size_t i=0; i<erreur->taille; i++)
    {
      free(erreur->messages[i]);
      erreur->messages[i] = NULL;
    }
}

/*
 *   Fonction :    empilementErreur
 *   
 *   Parametres :  pile_erreur_t* erreur, l'erreur a empiler.
 *                 char* message, le message de l'erreur.
 *
 *   Retour :    
 *                 
 *   Description : Empilement d'une pile d'erreur.
 */ 
void empilementErreur(pile_erreur_t* erreur, char* message)
{
  size_t longueurChaine =  strlen(message);
  erreur->messages[erreur->taille] = (char*) calloc(longueurChaine + 1, sizeof(char));
  strncpy(erreur->messages[erreur->taille], message, longueurChaine);
	
  erreur->taille++;
}

/*
 *   Fonction :    empilementErreur
 *   
 *   Parametres :  pile_erreur_t* erreur, pile d'erreur a vider.
 *   
 *   Retour :    
 *                 
 *   Description : Effacement des messages d'une pile d'erreur.
 */ 
void effacementErreur(pile_erreur_t* erreur)
{
  assert(erreur);
  suppressionErreur(erreur);
  creationErreur(erreur);
}

/*
 *   Fonction :    representationErreur
 *   
 *   Parametres :  pile_erreur_t* erreur, l'erreur a representer.
 *                 FILE* flux, flux vers lequel diriger la representation.
 *   
 *   Retour :    
 *                 
 *   Description : Representation d'une pile d'erreur.
 */ 
void representationErreur(pile_erreur_t* erreur, FILE* flux)
{
  if (erreur->taille == 0){return;}
  
  fprintf(flux
	  , "=> \e[31m%s\e[0m\n"
	  , erreur->messages[erreur->taille - 1]);
				
  for(size_t i=1; i<erreur->taille; i++)
    {
      fprintf(flux
	      , "-> \e[31m%s\e[0m\n"
	      , erreur->messages[erreur->taille - i - 1]);
    }
}

/*
 *   Fonction :    affichageErreur
 *   
 *   Parametres :  pile_erreur_t* erreur, l'erreur a afficher.
 *   
 *   Retour :    
 *                 
 *   Description : Affichage d'une pile d'erreur. Il s'agit d'une representation
 *                 de la chaine sur la sortie standard.
 */ 
void affichageErreur(pile_erreur_t* erreur)
{
  representationErreur(erreur, stderr);
  effacementErreur(erreur);
}

