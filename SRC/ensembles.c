#include <ensembles.h>

/*
 *   Fonction :    creationEnsemble
 *   
 *   Parametres :  ensemble_t* ensemble, l'ensemble disjoint a creer.
 *                 void* valeur, la valeur de l'ensemble disjoint.
 *   
 *   Retour :      
 *                 
 *   Description : Creation d'un ensemble disjoint.
 */
void creationEnsemble(ensemble_t* ensemble, void* valeur)
{
  ensemble->representant = ensemble;
  ensemble->valeur = valeur;
  ensemble->rang = 1;
}

/*
 *   Fonction :    unionEnsemble
 *   
 *   Parametres :  ensemble_t* ensemble, un premier ensemble disjoint.
 *                 ensemble_t* autreEnsemble, un second ensemble disjoint.
 *   
 *   Retour :      
 *                 
 *   Description : Realise l'union de deux ensembles disjoints.
 */
void unionEnsemble(ensemble_t* ensemble, ensemble_t* autreEnsemble)
{
  ensemble_t* representant = representantEnsemble(ensemble);
  ensemble_t* representantAutreEnsemble = representantEnsemble(autreEnsemble);

  if (representant->rang < representantAutreEnsemble->rang)
    {
      representant->representant = representantAutreEnsemble;
      representantAutreEnsemble->rang++;
    }
  else
    {
      representantAutreEnsemble->representant = representant;
      representant->rang++;
    }
}

/*
 *   Fonction :    representantEnsemble
 *   
 *   Parametres :  ensemble_t* ensemble, un ensemble disjoint.
 *   
 *   Retour :      ensemble_t*, le representant de l'ensemble disjoint.
 *                 
 *   Description : Acces au representant d'un ensemble disjoint.
 */
ensemble_t* representantEnsemble(ensemble_t* ensemble)
{
  ensemble_t* representant = ensemble;
  
  while(representant->representant != representant)
    {
      representant = representant->representant;
    }

  ensemble_t* iterateur = ensemble;

  while(iterateur != representant)
    {
      ensemble_t* ancien = iterateur->representant;
      iterateur->representant = representant;
      iterateur = ancien;
    }

  return representant;
}
