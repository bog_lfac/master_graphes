// Entetes systeme
#include <stdio.h>
#include <string.h>
// Entetes projet
#include <sauvegardes.h>

extern pile_erreur_t* ERREUR;

/*
 *   Fonction :    sauvegardeTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 char const* nomFichier, le nom du fichier de sauvegarde.
 *   
 *   Retour :      GRAPHE_OK si la sauvegarde s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Sauvegarde d'un TypGraphe dans un fichier.
 */   
erreur_t sauvegardeTypGraphe(TypGraphe* graphe, char const* nomFichier)
{
  // Le graphe doit exister.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de sauvegarder un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  // Ouverture du fichier.
  FILE* fichier = fopen(nomFichier, "w+");
  if (fichier == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible d'ouvrir le fichier de sauvegarde.");
      return GRAPHE_ERREUR;
    }

  representationTypGraphe(graphe, fichier);	
	
  // Fermeture du fichier.
  fclose(fichier);
	
  return GRAPHE_OK;
}

/*
 *   Fonction :    chargementTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, un graphe non initialise.
 *                 char const* nomFichier, le nom du fichier de chargement.
 *   
 *   Retour :      GRAPHE_OK si le chargement s'est bien deroule,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Chargement d'un TypGraphe dans un fichier.
 */ 
erreur_t chargementTypGraphe(TypGraphe* graphe, char const* nomFichier)
{
  // 'graphe' doit exister.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de charger un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  FILE* fichier = fopen(nomFichier, "r");
  if (fichier == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible d'ouvrir le fichier a charger.");
      return GRAPHE_ERREUR;
    }
	
  size_t const ligneTailleMax = 2048;
  char ligne[ligneTailleMax];
	
  int nbMaxSommets = lireNbMaxSommets(ligne, ligneTailleMax, fichier);
  if (nbMaxSommets == -1)
    {
      empilementErreur(ERREUR
		       , "Impossible de lire le nombre maximal de sommet.");
      fclose(fichier);
      return GRAPHE_ERREUR;
    }
	
  int estOriente = lireOrientation(ligne, ligneTailleMax, fichier);
  if (estOriente == -1)
    {
      empilementErreur(ERREUR
		       , "Impossible de lire l'orientation du graphe.");
      fclose(fichier);
      return GRAPHE_ERREUR;
    }	
	
  erreur_t err1 = creationTypGraphe(graphe, estOriente, nbMaxSommets);
  if (err1 == GRAPHE_ERREUR)
    {
      empilementErreur(ERREUR
		       , "Impossible de creer le graphe a charger.");
      fclose(fichier);
      return GRAPHE_ERREUR;
    }
	
  erreur_t err2 = chargementSommetsTypGraphe(graphe, ligne, ligneTailleMax, fichier);
  if (err2 == GRAPHE_ERREUR)
    {
      empilementErreur(ERREUR
		       , "Impossible de charger les sommets du graphe.");
      fclose(fichier);
      return GRAPHE_ERREUR;
    }

  fclose(fichier);
	
  return GRAPHE_OK;
}

/*
 *   Fonction :    lireLigne
 *   
 *   Parametres :  char* ligne, la ligne tampon pour stocker la prochaine ligne.
 *                 size_t tailleMax, la taille maximum de la ligne tampon.
 *                 FILE* flux, le flux a partir duquel la ligne sera lue. 
 *   
 *   Retour :      size_t, nombre total d'octets lus depuis le flux.  
 *                 
 *   Description : Lecture d'une ligne depuis un flux.
 */ 
size_t lireLigne(char* ligne, size_t tailleMax, FILE* flux)
{
  char caractereLu = ' ';
  size_t totalLu = 0;
  size_t lu = 0;
  size_t ligneIndice = 0;
	
  do
    {
      // Lecture d'un caractere depuis le flux d'entree.
      lu = fread(&caractereLu, sizeof(char), 1, flux);
      totalLu += lu;
		
      // Ajout du caractere lu dans 'ligne'.
      if (caractereLu != '\n')
	{
	  ligne[ligneIndice] = caractereLu;
	  ligneIndice++;
	}
		
      // En cas de depassement de la taille maximum autorisee par la ligne.
      if (ligneIndice >= tailleMax)
	{
	  ligne[tailleMax - 1] = '\0';
	  return tailleMax;
	}
    }
  while (lu > 0 && caractereLu != '\n');
	
  // Placement du caractere de fin de chaine.
  ligne[ligneIndice] = '\0';
	
  return totalLu;
}

/*
 *   Fonction :    lireNbMaxSommets
 *   
 *   Parametres :  char* ligne, la ligne tampon pour stocker les prochaines lignes.
 *                 size_t tailleMax, la taille maximum de la ligne tampon.
 *                 FILE* flux, le flux a partir duquel la ligne sera lue. 
 *   
 *   Retour :      int, le nombre maximum de sommets du graphe a creer.
 *                 -1, en cas d'erreur de lecture. 
 *                 
 *   Description : Lecture d'un nombre maximum de sommets depuis un flux.
 */ 
int lireNbMaxSommets(char* ligne, size_t tailleMax, FILE* flux)
{
  // Lecture du premier titre.
  lireLigne(ligne, tailleMax, flux);
	
  // Lecture du nombre maxmimum de sommets.
  lireLigne(ligne, tailleMax, flux);
	
  int nbMaxSommets = atoi(ligne);
	
  // Nombre invalide : on retourne un code d'erreur.
  if (nbMaxSommets == 0) { return -1; }
	
  return nbMaxSommets;
}

/*
 *   Fonction :    lireOrientation
 *   
 *   Parametres :  char* ligne, la ligne tampon pour stocker les prochaines lignes.
 *                 size_t tailleMax, la taille maximum de la ligne tampon.
 *                 FILE* flux, le flux a partir duquel la ligne sera lue. 
 *   
 *   Retour :      int, l'orientation du graphe a creer.
 *                 -1, en cas d'erreur de lecture. 
 *                 
 *   Description : Lecture de l'orientation d'un graphe depuis un flux.
 */ 
int lireOrientation(char* ligne, size_t tailleMax, FILE* flux)
{
  // Lecture du second titre.
  lireLigne(ligne, tailleMax, flux);
	
  // Lecture de l'orientation du graphe.
  lireLigne(ligne, tailleMax, flux);
	
  if (strcmp(ligne, "o") == 0)
    {
      return GRAPHE_ORIENTE;
    }
  else if (strcmp(ligne, "n") == 0)
    {
      return GRAPHE_NON_ORIENTE;
    }
  else
    {
      return -1;
    }
}

/*
 *   Fonction :    chargementTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, un graphe initialise.
 *                 char* ligne, la ligne tampon pour stocker les prochaines lignes.
 *                 size_t tailleMax, la taille maximum de la ligne tampon.
 *                 FILE* flux, le flux a partir duquel la ligne sera lue. 
 *   
 *   Retour :      GRAPHE_OK si le chargement des sommets s'est bien deroule,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Chargement des sommets d'un TypGraphe depuis un flux.
 */ 
erreur_t chargementSommetsTypGraphe(TypGraphe* graphe
				    , char* ligne
				    , size_t tailleMax
				    , FILE* flux)
{
  // 'graphe' doit exister.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de charger les sommets d'un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  // Lecture du troisieme titre.
  lireLigne(ligne, tailleMax, flux);
	
  while (lireLigne(ligne, tailleMax, flux) != 0)
    {	
      // 1- Identification du sommet.	
      int sommet = identificationNombre(ligne, 0, tailleMax);
      if (sommet == -1)
	{
	  empilementErreur(ERREUR
			   , "Impossible de lire le numero du sommet.");
	  return GRAPHE_ERREUR;
	}
		
      // 2- Insertion du sommet s'il n'a pas deja ete insere lors
      //    de la creation des aretes des sommets precedents.
      if (sommetTypGraphe(graphe, sommet) == NULL)
	{
	  erreur_t err = insertionSommetTypGraphe(graphe, sommet, sommet);
	  if (err == GRAPHE_ERREUR)
	    {
	      empilementErreur(ERREUR
			       , "Impossible d'inserer dans le graphe le sommet lu.");
	      return GRAPHE_ERREUR;
	    }
	}
		
      // 3- Chargement des aretes du sommet.
      erreur_t err2 = chargementAretes(graphe, ligne, tailleMax, sommet);
      if (err2 == GRAPHE_ERREUR)
	{
	  empilementErreur(ERREUR
			   , "Impossible de charger les aretes du sommet.");
	  return GRAPHE_ERREUR;
	}
    }
		
  return GRAPHE_OK;
}

/*
 *   Fonction :    chargementAretes
 *   
 *   Parametres :  TypGraphe* graphe, graphe dans lequel l'arete sera ajoutee.
 *                 char* ligne, la ligne tampon ou l'arete sera lue.
 *                 size_t tailleMax, la taille maximum de la ligne tampon.
 *                 size_t sommet, le sommet pour lequel creer les aretes.
 *   
 *   Retour :      GRAPHE_OK si le chargement de l'arete s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Lecture d'une arete d'un graphe depuis un flux.
 */ 
erreur_t chargementAretes(TypGraphe* graphe
                          , char* ligne
                          , size_t tailleMax
                          , size_t sommet)
{
  // 'graphe' doit exister.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de charger les aretes d'un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  size_t indiceLigne = 0;

  while (ligne[indiceLigne] != '\0')
    {
      // 1- On se deplace jusqu'a une parenthese ouvrante.
      while(ligne[indiceLigne] != '\0' && ligne[indiceLigne] != '(')
	{
	  indiceLigne++;
	}		
      if (ligne[indiceLigne] == '\0') { continue; }
	
      // 2- Identification du successeur.
      indiceLigne++;
      int sommet2 = identificationNombre(ligne, indiceLigne, tailleMax);
		
      // 3- Insertion du successeur si necessaire.
      if (sommetTypGraphe(graphe, sommet2) == NULL)
	{
	  insertionSommetTypGraphe(graphe, sommet2, sommet2);
	}
		
      // 4- On se deplace jusqu'a un slash.
      while(ligne[indiceLigne] != '\0' && ligne[indiceLigne] != '/')
	{
	  indiceLigne++;
	}
      if (ligne[indiceLigne] == '\0') { continue; }
	
      // 5- Identification du poids.
      indiceLigne++;
      int poids = identificationNombre(ligne, indiceLigne, tailleMax);
	
      // 6- On cree l'arete.
      erreur_t err = insertionAreteTypGraphe(graphe, sommet, sommet2, poids);
      if (err == GRAPHE_ERREUR)
	{
	  empilementErreur(ERREUR
			   , "Impossible d'inserer dans le graphe l'arete lue.");
	  return GRAPHE_ERREUR;
	}
    }

  return GRAPHE_OK;
}

/*
 *   Fonction :    identificationNombre
 *   
 *   Parametres :  char* ligne, la ligne tampon ou le nombre sera lu.
 *                 size_t indice, indice a partir duquel identifier un nombre
 *                                dans 'ligne'.
 *                 size_t tailleMax, la taille maximum de la ligne tampon.
 *   
 *   Retour :      int, le nombre a identifier.
 *                 -1, en cas d'erreur de lecture. 
 *                 
 *   Description : Lecture d'un nombre depuis une chaine a partir d'un indice.
 */ 
int identificationNombre(char* ligne, size_t indice, size_t tailleMax)
{
  size_t const tamponNombreTailleMax = 32;
  char tamponNombre[tamponNombreTailleMax];
		
  char caractereLu;
  size_t indiceLigne = indice;
  size_t indiceTampon = 0;
  int nombreTrouve = 0;
		
  do
    {
      caractereLu = ligne[indiceLigne];
      indiceLigne++;
		
      if (caractereLu >= '0' && caractereLu <= '9')
	{
	  tamponNombre[indiceTampon] = caractereLu;
	  indiceTampon++;
	}
      else
	{
	  nombreTrouve = 1;
	  tamponNombre[indiceTampon] = '\0';
	}
    }
  while (nombreTrouve == 0 
	 && indiceLigne < tailleMax
	 && indiceTampon < tamponNombreTailleMax);
	
  int nombre = atoi(tamponNombre);
	
  if (nombre == 0) { return -1; }
  return nombre;
}

