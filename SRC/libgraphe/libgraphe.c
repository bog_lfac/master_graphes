// Entetes systeme
#include <stdio.h>
#include <assert.h>
// Entetes projet
#include <libgraphe.h>
#include <coordonnees.h>
#include <ensembles.h>
#include <libtas.h>

extern pile_erreur_t* ERREUR;

/*
 *   Fonction :    creationTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe a creer.
 *  			   int estOriente, 0 si le graphe n'est pas oriente.
 *  			   size_t nbMaxSommet, le nombre maximal de sommets du graphe.
 *   
 *   Retour :      GRAPHE_OK si la creation s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Creation du TypGraphe et initialisation de ses champs.
 */
erreur_t creationTypGraphe(TypGraphe* graphe
			   , int estOriente
			   , size_t nbMaxSommets)
{
  // Le graphe doit exister en memoire.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR, "Impossible de creer un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  // La taille du graphe doit etre realiste
  if (nbMaxSommets < 1
      || nbMaxSommets > GRAPHE_TAILLE_MAX)
    {
      empilementErreur(ERREUR
		       , "Impossible de creer un graphe de cette taille.");
      return GRAPHE_ERREUR;
    }
	
  // L'orientation doit valoir 0 ou 1.
  if (estOriente < 0 || estOriente > 1)
    {
      empilementErreur(ERREUR
		       , "Impossible de creer un graphe avec cette orientation.");
      return GRAPHE_ERREUR;
    }
	
  graphe->estOriente = estOriente;
  graphe->nbMaxSommets = nbMaxSommets;
  graphe->nbSommets = 0;
  graphe->nbAretes = 0;
  
  graphe->listesAdjacences = (TypVoisins**) calloc(nbMaxSommets, sizeof(TypVoisins*));
	
  for (size_t i=0; i<graphe->nbMaxSommets; i++)
    {
      graphe->listesAdjacences[i] = NULL;
    }
	
  return GRAPHE_OK;
}

int comparerTasArete(void* a1, void* a2)
{
  tas_arete_t* arete1 = (tas_arete_t*) a1;
  tas_arete_t* arete2 = (tas_arete_t*) a2;
  
  return arete2->poids - arete1->poids;
}

/*
 *   Fonction :    creationArpmTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe a partir duquel creer l'ARPM.
 *                 TypGraphe* arpm, l'ARPM issu du graphe.
 *   
 *   Retour :      GRAPHE_OK si la creation s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Creation d'un ARPM a partir d'un graphe.
 */
erreur_t creationArpmTypGraphe(TypGraphe* graphe, TypGraphe* arpm)
{
  creationTypGraphe(arpm, GRAPHE_NON_ORIENTE, graphe->nbSommets);

  // Creation des ensembles de sommets
  ensemble_t ensemblesSommets[graphe->nbSommets];
  int valeursSommets[graphe->nbSommets];

  // Creation des ensembles pour chaques sommets.
  for (size_t i=0; i<graphe->nbSommets; i++)
    {
      valeursSommets[i] = i+1;
      creationEnsemble(ensemblesSommets+i, valeursSommets+i);
    }
  
  // Ajouts des aretes dans le tas
  // et creation des sommets dans l'arpm.
  size_t aretesTaille = 0;
  tas_arete_t* aretes[graphe->nbAretes];
  
  for(size_t i=0; i<graphe->nbAretes; i++) {aretes[i] = NULL;}
  
  for (size_t sommet=0; sommet<graphe->nbSommets; sommet++)
    {
      // Sommet arpm.
      insertionSommetTypGraphe( arpm
				, sommet+1
				, valeurSommetTypGraphe(graphe, sommet+1) );
      
      // Aretes tas.
      TypVoisins* iterateur = graphe->listesAdjacences[sommet];
      if (iterateur->voisinSuivant == iterateur) {continue;}
      
      TypVoisins* sentinelle = iterateur;
      iterateur = iterateur->voisinSuivant;
      
      while (iterateur != sentinelle)
	{	  
	  tas_arete_t* arete = (tas_arete_t*) malloc(sizeof(tas_arete_t));
	  arete->u = sommet + 1;
	  arete->v = iterateur->voisin;
	  arete->poids = iterateur->poids;
	  aretes[aretesTaille] = arete;
	  aretesTaille++;
	  iterateur = iterateur->voisinSuivant;
	}
    }

  tas_construction((void**)aretes, aretesTaille, comparerTasArete);
    
  int nbSommetsAjoutes = 0;

  while (nbSommetsAjoutes < graphe->nbSommets && aretesTaille > 0)
    {
      // Recuperation de la meilleure arete.
      tas_arete_t* arete = tas_extraction((void**)aretes, &aretesTaille, comparerTasArete);
      
      // On verifie que l'arete ne cree pas de cycle
      if( representantEnsemble(ensemblesSommets + arete->u - 1)
      	  != representantEnsemble(ensemblesSommets + arete->v - 1) )
	{
	  insertionAreteSymetriqueTypGraphe(arpm, arete->u, arete->v, arete->poids);
	  unionEnsemble(ensemblesSommets + arete->u - 1, ensemblesSommets + arete->v - 1);
	  nbSommetsAjoutes++;
	}

      free(arete);
    }
  
  
  return GRAPHE_OK;
}

/*
 *   Fonction :    suppressionTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe a supprimer
 *   
 *   Retour :      GRAPHE_OK si la suppression s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Suppression du TypGraphe et de ses champs.
 */
erreur_t suppressionTypGraphe(TypGraphe* graphe)
{
  // Le graphe doit exister en memoire.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  for (size_t i=0; i<graphe->nbMaxSommets; i++)
    {
	 	
      if (graphe->listesAdjacences[i] != NULL)
	{
	  erreur_t err = suppressionSommetTypGraphe(graphe, i + 1);
			
	  if (err != GRAPHE_OK)
	    {
	      empilementErreur(ERREUR
			       , "Impossible de supprimer un sommet du graphe.");
	      return GRAPHE_ERREUR;
	    }
	}
    }
	
  free(graphe->listesAdjacences);
  graphe->listesAdjacences = NULL;
	
  return GRAPHE_OK;
}

/*
 *   Fonction :    sommetTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *  			   int sommet, un sommet du graphe.
 *   
 *   Retour :      TypVoisins*, la liste d'adjacence du sommet. 
 *                 NULL si le sommet n'existe pas.
 *                 
 *   Description : Acces au sommet d'un TypGraphe.
 */
TypVoisins* sommetTypGraphe(TypGraphe* graphe, size_t sommet)
{
  // Le graphe doit exister en memoire.
  if (graphe == NULL) 
    {
      return NULL; 
    }
	
  // Le sommet doit exister dans le graphe.
  if (sommet > graphe->nbMaxSommets
      || sommet == 0) 
    {
      return NULL; 
    }
	
  return graphe->listesAdjacences[sommet - 1];
}

/*
 *   Fonction :    valeurSommetTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 size_t sommet, le sommet dont on veut la valeur.
 *   
 *   Retour :      int, la valeur du sommet
 *                 -1, en cas d'erreur.
 *                 
 *   Description : Recuperation de la valeur d'un sommet dans un
 *                 TypGraphe.
 */
int valeurSommetTypGraphe(TypGraphe* graphe, size_t sommet)
{
  // Le graphe doit exister en memoire.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de recuperer la valeur d'un sommet "
		       "dans un graphe non initialise.");
      
      return GRAPHE_ERREUR;
    }
	
  // Le sommet doit etre dans le graphe.
  if (sommet == 0 || sommet > graphe->nbMaxSommets)
    {
      empilementErreur(ERREUR
		       , "Impossible de recuperer la valeur d'un sommet en dehors du graphe.");
      return GRAPHE_ERREUR;
    }

  TypVoisins* sentinelle = premierTypVoisins(graphe->listesAdjacences[sommet - 1]);
	  
  int valeur = sommet;

  if (sentinelle != NULL)
    {
      valeur = (int)poidsTypVoisins(sentinelle);
      return valeur;
    }
  
  return -1;
}

/*
 *   Fonction :    insertionSommetTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 size_t sommet, le sommet a ajouter.
 *                 int valeur, la valeur du sommet
 *   
 *   Retour :      GRAPHE_OK, si l'insertion a reussie.
 *                 GRAPHE_ERREUR si l'insertion a echouee.
 *                 
 *   Description : Insertion d'un sommet dans un TypGraphe.
 */
erreur_t insertionSommetTypGraphe(TypGraphe* graphe, size_t sommet, int valeur)
{
  // Le graphe doit exister en memoire.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible d'inserer un sommet dans un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  // Le sommet doit etre dans le graphe.
  if (sommet == 0 || sommet > graphe->nbMaxSommets)
    {
      empilementErreur(ERREUR
		       , "Impossible d'inserer un sommet en dehors du graphe.");
      return GRAPHE_ERREUR;
    }
	
	
  // Le sommet ne doit pas deja exister.
  if (graphe->listesAdjacences[sommet - 1] != NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible d'inserer un sommet deja existant.");
      return GRAPHE_ERREUR;
    }
	
  graphe->listesAdjacences[sommet - 1] = (TypVoisins*) malloc( sizeof(TypVoisins) );
	
  // Le poids de la sentinelle est la valeur du sommet.
  erreur_t err = creationTypVoisins(graphe->listesAdjacences[sommet - 1]
				    , VALEUR_SENTINELLE
				    , (int) valeur);
	
  if (err != VOISIN_OK)
    {
      empilementErreur(ERREUR
		       , "Impossible de creer la liste d'adjacence du sommet a inserer.");
      return GRAPHE_ERREUR; 
    }

  graphe->nbSommets++;
  
  return GRAPHE_OK;  
}

TypVoisins* areteTypGraphe(TypGraphe* graphe
			   , size_t predecesseur
			   , size_t successeur)
{
  // Le graphe doit exister.
  if (graphe == NULL)
    {
      return NULL;
    }
	
  // Le sommet predecesseur doit exister.
  if (sommetTypGraphe(graphe, predecesseur) == NULL)
    {
      return NULL;
    }

  // Le sommet successeur doit exister.
  if (sommetTypGraphe(graphe, successeur) == NULL)
    {
      return NULL;
    }

  return rechercheValeurTypVoisins(sommetTypGraphe(graphe, predecesseur)
				   , successeur);  
}

/*
 *   Fonction :    insertionAreteTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 size_t predecesseur, le sommet d'ou part l'arete.
 *                 size_t successeur, le sommet ou arrive l'arete.
 *                 double poids, le poids de l'arete.
 *
 *   Retour :      GRAPHE_OK si l'insertion s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Insertion d'une arete dans un TypGraphe.
 */
erreur_t insertionAreteTypGraphe(TypGraphe* graphe
				 , size_t predecesseur
				 , size_t successeur
				 , double poids)
{
  // Le graphe doit exister.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible d'inserer une arete dans un graphe non initialise.");       
      return GRAPHE_ERREUR;
    }
	
  // Le sommet predecesseur doit exister.
  if (sommetTypGraphe(graphe, predecesseur) == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible d'inserer une arete dont le predecesseur n'existe pas.");       
      return GRAPHE_ERREUR;
    }

  // Le sommet successeur doit exister.
  if (sommetTypGraphe(graphe, successeur) == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible d'inserer une arete dont le successeur n'existe pas.");       
      return GRAPHE_ERREUR;
    }
	
  // L'arete ne doit pas deja exister.
  TypVoisins* arete;
  arete = rechercheValeurTypVoisins(sommetTypGraphe(graphe, predecesseur)
				    , successeur);
  if (arete != NULL) 
    {
      empilementErreur(ERREUR
		       , "Impossible d'inserer une arete deja existante dans le graphe."); 
      return GRAPHE_ERREUR;
    }
	
  TypVoisins* resultat;
  resultat = creationQueueTypVoisins(graphe->listesAdjacences[predecesseur - 1]
				     , successeur
				     , poids);
  if (resultat == NULL)
    {
      empilementErreur(ERREUR, "Impossible d'inserer l'arete dans le graphe."); 
      return GRAPHE_ERREUR;
    }

  graphe->nbAretes++;
  
  return GRAPHE_OK;
}

/*
 *   Fonction :    insertionAreteSymetriqueTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 size_t sommet1, un sommet de l'arete.
 *                 size_t sommet2, l'autre sommet de l'arete.
 *                 double poids, le poids de l'arete.
 *
 *   Retour :      GRAPHE_OK si l'insertion s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Insertion d'une arete symetrique dans un TypGraphe.
 */
erreur_t insertionAreteSymetriqueTypGraphe(TypGraphe* graphe
					   , size_t sommet1
                                           , size_t sommet2
					   , double poids)
{
  erreur_t err1 = insertionAreteTypGraphe(graphe, sommet1, sommet2, poids);	
  if (err1 == VOISIN_ERREUR)
    {
      empilementErreur(ERREUR
		       , "Impossible de d'inserer une arete allant du premier sommet vers le second."); 
      return GRAPHE_ERREUR;
    }
	
  erreur_t err2 = insertionAreteTypGraphe(graphe, sommet2, sommet1, poids);
  if (err2 == VOISIN_ERREUR)
    {
      empilementErreur(ERREUR
		       , "Impossible de d'inserer une arete allant du second sommet vers le premier."); 
      return GRAPHE_ERREUR;
    }
	
  return GRAPHE_OK;
}                                      

/*
 *   Fonction :    suppressionSommetTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 size_t sommet, le sommet a supprimer.
 *   
 *   Retour :      GRAPHE_OK si la suppression s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Suppression du sommet d'un TypGraphe.
 */
erreur_t suppressionSommetTypGraphe(TypGraphe* graphe, size_t sommet)
{
  // Le graphe doit etre defini
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer un sommet dans un graphe non initialise."); 
      return GRAPHE_ERREUR;
    }
	
  // Le sommet doit exister dans le graphe	
  if (sommetTypGraphe(graphe, sommet) == NULL)
    { 
      empilementErreur(ERREUR,
		       "Impossible de supprimer un sommet qui n'existe pas.");
      return GRAPHE_ERREUR;
    }

  // 1- Suppression des aretes liees au sommet.
  for (size_t s=1; s<=graphe->nbMaxSommets; s++)
    {
      // On ne considere que les sommets existants dans le graphe.
      TypVoisins* sommetCible = sommetTypGraphe(graphe, s);
		
		
      if (sommetCible != NULL)
	{
	  TypVoisins* sentinelle = premierTypVoisins(sommetCible);

	  TypVoisins* iterateur;
	  iterateur = sentinelle->voisinSuivant;
			
	  while (iterateur != sentinelle)
	    {
	      if (valeurTypVoisins(iterateur) == (int) sommet)
		{
		  // On avance l'iterateur avant suppression pour ne pas
		  // l'invalider.
					
		  iterateur = suivantTypVoisins(iterateur);	
	  
		  erreur_t err1 = suppressionAreteTypGraphe(graphe, s, sommet);
					
		  if (err1 != GRAPHE_OK)
		    {
		      empilementErreur(ERREUR, "Impossible de supprimer une arete.");
		      return GRAPHE_ERREUR;
		    }
		}
	      else
		{
		  iterateur = suivantTypVoisins(iterateur); 
		}
	    }
	}
    }
	
  // 2- Suppression du sommet.
  erreur_t err2 = suppressionTypVoisins(graphe->listesAdjacences[sommet - 1]);
  free(graphe->listesAdjacences[sommet - 1]);	
  graphe->listesAdjacences[sommet - 1] = NULL;
  graphe->nbSommets--;
  
  if (err2 == GRAPHE_ERREUR)
    {
      empilementErreur(ERREUR, "Impossible de supprimer le sommet dans le graphe.");
      return GRAPHE_ERREUR;
    }
	
  
  return GRAPHE_OK;
}

/*
 *   Fonction :    suppressionAreteTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 size_t predecesseur, le sommet d'ou part l'arete.
 *                 size_t successeur, le sommet ou arrive l'arete.
 *   
 *   Retour :      GRAPHE_OK si la suppression s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Suppression d'une arete d'un TypGraphe.
 */
erreur_t suppressionAreteTypGraphe(TypGraphe* graphe
                                   , size_t predecesseur
                                   , size_t successeur)
{
  // Le graphe doit exister.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer une arete dans un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
	
  // Le sommet predecesseur doit exister.
  if (sommetTypGraphe(graphe, predecesseur) == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer une arete dont le predecesseur n'existe pas.");
      return GRAPHE_ERREUR;
    }
	
  // Le sommet successeur doit exister.
  if (sommetTypGraphe(graphe, successeur) == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer une arete dont le successeur n'existe pas.");
      return GRAPHE_ERREUR;
    }
	
  // Recherche de l'arete dans la liste d'adjacence du predecesseur.
  TypVoisins* arete;
  arete = rechercheValeurTypVoisins(sommetTypGraphe(graphe
						    , predecesseur)
				    , successeur);

  // L'arete doit exister dans le graphe.
  if (arete == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer une arete qui n'existe pas.");
      return GRAPHE_ERREUR;
    }

  // Suppression de l'arete dans la liste d'adjacence.
  erreur_t err = suppressionVoisinTypVoisins(arete);

  if (err != VOISIN_OK)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer une arete dans le graphe.");
      return GRAPHE_ERREUR;
    }
  
  graphe->nbAretes--;
  
  return GRAPHE_OK;
}

/*
 *   Fonction :    suppressionAreteSymetriqueTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 size_t sommet1, un sommet de l'arete.
 *                 size_t sommet2, l'autre sommet de l'arete.
 *   
 *   Retour :      GRAPHE_OK si la suppression s'est bien deroulee,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Suppression d'une arete symetrique d'un TypGraphe.
 */                               
erreur_t suppressionAreteSymetriqueTypGraphe(TypGraphe* graphe
					     , size_t sommet1
					     , size_t sommet2)
{
  // Les deux aretes doivent exister.
  TypVoisins* arete1;
  TypVoisins* arete2;
  arete1 = rechercheValeurTypVoisins(sommetTypGraphe(graphe
						     , sommet1)
				     , sommet2);
	                                   
  arete2 = rechercheValeurTypVoisins(sommetTypGraphe(graphe
						     , sommet2)
				     , sommet1);
	
  if (arete1 == NULL || arete2 == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer symetriquement une arete non symetrique.");
      return GRAPHE_ERREUR;
    }
	
  erreur_t err1 = suppressionAreteTypGraphe(graphe, sommet1, sommet2);
  if (err1 == VOISIN_ERREUR)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer symetriquement la premiere arete.");
      return GRAPHE_ERREUR;
    }
	
  erreur_t err2 = suppressionAreteTypGraphe(graphe, sommet2, sommet1);
  if (err2 == VOISIN_ERREUR)
    {
      empilementErreur(ERREUR
		       , "Impossible de supprimer symetriquement la seconde arete.");
      return GRAPHE_ERREUR;
    }
	
  return GRAPHE_OK;
}

/*
 *   Fonction :    representationTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *                 FILE* sortie, sortie utilisee.
 *   
 *   Retour :      GRAPHE_OK si l'affichage s'est bien deroule,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Affichage d'un TypGraphe sur un flux.
 */                                            
erreur_t representationTypGraphe(TypGraphe* graphe, FILE* sortie)
{
  // Le graphe doit exister en memoire.
  if (graphe == NULL)
    {
      empilementErreur(ERREUR
		       , "Impossible de representer un graphe non initialise.");
      return GRAPHE_ERREUR;
    }
    
  fprintf(sortie, "# nombre maximum de sommets\n");
  fprintf(sortie, "%zd\n", graphe->nbMaxSommets);
  fprintf(sortie, "# oriente\n");
  fprintf(sortie, "%c\n", graphe->estOriente ? 'o' : 'n');
  fprintf(sortie, "# sommets : voisins\n");
    
  for (size_t sommet=0; sommet<graphe->nbMaxSommets; sommet++)
    {
      TypVoisins* voisin = graphe->listesAdjacences[sommet];

      if (voisin != NULL)
    	{	  
	  int valeur = valeurSommetTypGraphe(graphe, sommet + 1);
	  
	  if (valeur != (int) sommet)
	    {
	      fprintf(sortie, "%zd (%d, %d) : "
		      , sommet + 1
		      , coordonneeX(valeur)
		      , coordonneeY(valeur));
	    }
	  else
	    {
	      fprintf(sortie, "%zd : "
		      , sommet + 1);
	  
	    }
	  
	  erreur_t err = representationTypVoisins(voisin, sortie);
	  fprintf(sortie, "\n");
    		
	  if (err == VOISIN_ERREUR)
	    {
	      empilementErreur(ERREUR
			       , "Impossible de representer les aretes d'un sommet du graphe.");
	      return GRAPHE_ERREUR;
	    }
    	}    	    	
    }
    
  return GRAPHE_OK;	
}
 
/*
 *   Fonction :    affichageTypGraphe
 *   
 *   Parametres :  TypGraphe* graphe, le graphe utilise.
 *   
 *   Retour :      GRAPHE_OK si l'affichage s'est bien deroule,
 *                 GRAPHE_ERREUR sinon.
 *                 
 *   Description : Affichage d'un TypGraphe.
 */                                            
erreur_t affichageTypGraphe(TypGraphe* graphe)
{
  // Le graphe doit exister en memoire.
  assert(graphe);
    
  erreur_t err = representationTypGraphe(graphe, stdout);
    	
  if (err != GRAPHE_OK)
    {
      empilementErreur(ERREUR
		       , "Impossible d'afficher un graphe non representable.");
      return GRAPHE_ERREUR;
    }
    
  return GRAPHE_OK;
}

