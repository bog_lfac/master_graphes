// Entetes systeme
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// Entetes projet
#include <libgraphe.h>
#include <libtas.h>
#include <coordonnees.h>
#include <erreur.h>
#include <pvc.h>
#include <ensembles.h>

extern pile_erreur_t* ERREUR;

int est_plus_grand(void* a, void* b)
{
  int* aa = (int*) a;
  int* bb = (int*) b;

  return *aa - *bb;
}


int hasard (int inf, int sup)
{
  // Retourne un nombre pseudo-aleatoire
  // sur l'intervalle [inf; sup[.
  return rand()%(sup - inf) + inf;
}

int main (int argc, char** argv)
{
  char* help = "Usage : \n\t"
    "prog_pvc <graine> <nombre de sommets> <algorithme de résolution>\n"
    "Algorithmes : "
    "\n\t--opti, solution optimale"
    "\n\t--ppv, heuristique plus proche voisin"
    "\n\t--ppd, heuristique du plus petit détour"
    "\n\t--arpm, heuristique basée sur un ARPM"
    "\e[0m\n";

  // Recuperation des parametres du programme.
  if (argc != 4)
    {
      fprintf(stderr, "\e[31mNombre de paramètre invalide.\n%s" , help);
      return 1;
    }

  TypGraphe graphe;
  ERREUR = (pile_erreur_t*) malloc(sizeof(pile_erreur_t));
  creationErreur(ERREUR);
  
  int graine = atoi(argv[1]);
  int nombreDeSommets = atoi(argv[2]);

  // Recuperation de l'algorithme de resolution.
  solution_fct pvc_solution = NULL;
  char* pvc_titre;
  
  if (strcmp(argv[3], "--opti") == 0)
    {
      pvc_solution = pvc_optimal;
      pvc_titre = "Solution optimale";
    }
  if (strcmp(argv[3], "--ppv") == 0)
    {
      pvc_solution = pvc_plus_proche_voisin;
      pvc_titre = "Heuristique du plus proche voisin";
    }
  if (strcmp(argv[3], "--ppd") == 0)
    {
      pvc_solution = pvc_plus_petit_detour;
      pvc_titre = "Heuristique du plus petit détour";
    }
  if (strcmp(argv[3], "--arpm") == 0)
    {
      pvc_solution = pvc_arpm;
      pvc_titre = "Heuristique basée sur ARPM";
    }
    
  if (nombreDeSommets <= 0)
    {
      fprintf(stderr, "\e[31mNombre de sommets invalide (= %d).\n%s", nombreDeSommets, help);
      return 1;
    }

  if (pvc_solution == NULL)
    {
      fprintf(stderr, "\e[31mAlgorithme de resolution inconnu (= %s).\n%s", argv[3], help);
      return 1;
    }

  // Initialisation de la graine.
  srand(graine);

  
  // Génération des sommets.
  {
    creationTypGraphe(&graphe, GRAPHE_NON_ORIENTE, nombreDeSommets);

    for(int sommet=1; sommet<=nombreDeSommets; sommet++)
      {
  	int coord = coordonnee2D( hasard(0, 101), hasard(0, 101) );
  	insertionSommetTypGraphe(&graphe, sommet, coord);
      }

    // Creation des aretes.
    for(int s1=1; s1<=nombreDeSommets; s1++)
      {
  	for(int s2=1; s2<=nombreDeSommets; s2++)
  	  {
  	    if (s1 >= s2) {continue;}
	    
  	    insertionAreteSymetriqueTypGraphe( &graphe
  	    				       , s1
  	    				       , s2
  	    				       , distance2D(valeurSommetTypGraphe(&graphe, s1)
  	    						    , valeurSommetTypGraphe(&graphe, s2)) );
  	  }
      }
    
    affichageTypGraphe(&graphe);
    printf("\n");
  }

  int parcours[nombreDeSommets+1];
  memset(parcours, -1, sizeof(int)*(nombreDeSommets+1));
  
  // Calcul de la solution.
  double cout = pvc_solution (&graphe, parcours);

  // Affichage de la solution.
  printf("Parcours trouvé de coût %0.1f.\n", cout);
  for(size_t i=0; i<nombreDeSommets+1; i++)
    {
      printf("%d ", parcours[i]);
    }
  printf("\n");

  exportationParcours(&graphe, parcours, "script.py", pvc_titre);
  
  affichageErreur(ERREUR);
  // Suppression
  suppressionTypGraphe(&graphe);
  free(ERREUR);

  return 0;
}

