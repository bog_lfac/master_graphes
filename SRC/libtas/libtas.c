// Entetes systeme
#include <stdio.h>
// Entetes projet
#include <libtas.h>

/*
 *   Fonction :    tas_pere
 *   
 *   Parametres :  size_t indice, indice du noeud dont on veut le pere.
 *   
 *   Retour :      size_t, l'indice du pere de l'indice donne.
 *                 
 *   Description : Calcule de l'indice pere d'un noeud du tas.
 */
size_t tas_pere(size_t indice)
{
  return (indice - 1)/2;
}

/*
 *   Fonction :    tas_fils_gauche
 *   
 *   Parametres :  size_t indice, indice du noeud dont on veut le fils gauche.
 *   
 *   Retour :      size_t, l'indice du fils gauche de l'indice donne.
 *                 
 *   Description : Calcule de l'indice fils gauche d'un noeud du tas.
 */
size_t tas_fils_gauche(size_t indice)
{
  return (indice * 2) + 1;
}

/*
 *   Fonction :    tas_fils_droit
 *   
 *   Parametres :  size_t indice, indice du noeud dont on veut le fils droit.
 *   
 *   Retour :      size_t, l'indice du fils droit de l'indice donne.
 *                 
 *   Description : Calcule de l'indice fils droit d'un noeud du tas.
 */
size_t tas_fils_droit(size_t indice)
{
  return (indice + 1) * 2;
}

/*
 *   Fonction :    tas_entassement
 *   
 *   Parametres :  void** tableau, le tas a considerer.
 *                 size_t taille, la taille du tas.
 *                 compare_fct comparer, la fonction indiquant la relation d'ordre 
 *                                       entre les elements du tas.
 *                 size_t indice, indice de l'element du tas a entasser.
 *                 
 *   
 *   Retour :      
 *                 
 *   Description : Entassement d'un element du tas.
 */
void tas_entassement(void** tableau, size_t taille, compare_fct comparer, size_t indice)
{
  size_t fils_gauche = tas_fils_gauche(indice);
  size_t fils_droit = tas_fils_droit(indice);

  size_t max = indice;
  	
  if ( fils_gauche < taille
	&& comparer(tableau[fils_gauche], tableau[max]) > 0 )
    {
      max = fils_gauche;
    }

  if ( fils_droit < taille
       && comparer(tableau[fils_droit], tableau[max]) > 0 )
    {
      max = fils_droit;
    }

  if (indice != max)
  {
    void* tmp = tableau[indice];
    tableau[indice] = tableau[max];
    tableau[max] = tmp;

    tas_entassement(tableau, taille, comparer, max);
  }  
}

/*
 *   Fonction :    tas_construction
 *   
 *   Parametres :  void** tableau, le tas a considerer.
 *                 size_t taille, la taille du tas.
 *                 compare_fct comparer, la fonction indiquant la relation d'ordre 
 *                                       entre les elements du tas.
 *                 
 *   
 *   Retour :      
 *                 
 *   Description : Construction d'un tas dans un tableau donnee.
 */
void tas_construction(void** tableau, size_t taille, compare_fct comparer)
{
  for(size_t indice = taille/2; indice != -1; indice--)
    {
      tas_entassement(tableau, taille, comparer, indice);
    }
}

/*
 *   Fonction :    tas_extraction
 *   
 *   Parametres :  void** tableau, le tas a considerer.
 *                 size_t taille, la taille du tas.
 *                 compare_fct comparer, la fonction indiquant la relation d'ordre 
 *                                       entre les elements du tas.
 *                 
 *   
 *   Retour :      void*, l'element du tas extrait.
 *                 
 *   Description : Extraction de l'element prioritaire relativement a
 *                 la relation d'ordre entre les elements du tas.
 */
void* tas_extraction(void** tableau, size_t* taille, compare_fct comparer)
{
  if (*taille < 1) { return NULL; }

  void* meilleur = tableau[0];
  
  tableau[0] = tableau[*taille - 1];

  tas_entassement(tableau, *taille, comparer, 0);

  (*taille)--;

  return meilleur;
}

/*
 *   Fonction :    tas_ajout
 *   
 *   Parametres :  void** tableau, le tas a considerer.
 *                 size_t taille, la taille du tas.
 *                 compare_fct comparer, la fonction indiquant la relation d'ordre 
 *                                       entre les elements du tas.
 *                 void* element, l'element a ajouter dans le tas.
 *                 
 *   
 *   Retour :      
 *                 
 *   Description : Ajout d'un element dans le tas.
 */
void tas_ajout(void** tableau, size_t* taille, compare_fct comparer, void* element)
{
  tableau[*taille] = element;
  tas_construction(tableau, *taille, comparer);
  (*taille)++;
}
