#include <coordonnees.h>
#include <math.h>

/*
 *   Fonction :    affichageCoordonneeFormatBinaire
 *   
 *   Parametres :  int coordonnee
 *                 
 *   
 *   Retour :      
 *                 
 *   Description : Affichage en codage binaire d'une coordonnee.
 */
void affichageCoordonneeFormatBinaire(int coordonnee)
{
  for(size_t i=0; i<32; i++)
    {
      printf("%d.", ((coordonnee & (1<<(32-i-1))) !=0));
      if(i%4 == 3) {printf(" ");}
    }
  
  printf("\n");
}

/*
 *   Fonction :    coordonnee2D
 *   
 *   Parametres :  unsigned short int x, l'abscisse de la coordonnee
 *                 unsigned short int x, l'ordonnee de la coordonnee.
 *                 
 *   
 *   Retour :      
 *                 
 *   Description : Conversion des composantes d'un couple sur R2 en un
 *                 entier sur 32 bits.
 */
unsigned int coordonnee2D(unsigned short int x, unsigned short int y)
{
  unsigned int coordonnee = x;
  coordonnee = coordonnee | y<<16;

  
  return coordonnee;
}

/*
 *   Fonction :    coordonneeX
 *   
 *   Parametres : unsigned int coordonnee, la coordonnee dont on
 *                veut l'abscisse.
 *                 
 *   
 *   Retour :     unsigned short int, la valeur de l'abscisse.
 *                 
 *   Description : Acces a l'abscisse d'une coordonnee.
 */
unsigned short int coordonneeX(unsigned int coordonnee)
{
  return (unsigned int)(coordonnee);
}

/*
 *   Fonction :    coordonneeY
 *   
 *   Parametres : unsigned int coordonnee, la coordonnee dont on
 *                veut l'ordonnee.
 *                 
 *   
 *   Retour :     unsigned short int, la valeur de l'ordonnee.
 *                 
 *   Description : Acces a l'ordonnee d'une coordonnee.
 */
unsigned short int coordonneeY(unsigned int coordonnee)
{
  return (unsigned int)(coordonnee>>16);
}

/*
 *   Fonction :    distance2D
 *   
 *   Parametres : unsigned int coordonnee1, premier point du plan.
 *                unsigned int coordonnee2, second point du plan.
 *   
 *   Retour :     float, la distance euclidienne entre les deux points du plan.
 *                 
 *   Description : Calcul de la distance euclidienne entre deux coordonnees.
 */
float distance2D(unsigned int coordonnee1, unsigned int coordonnee2)  
{
  return sqrt( pow(coordonneeX(coordonnee1) - coordonneeX(coordonnee2), 2)
	       + pow(coordonneeY(coordonnee1) - coordonneeY(coordonnee2), 2) );
}
