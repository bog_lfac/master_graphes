// Entete systeme
#include <string.h>
// Entete projet
#include <pvc.h>
#include <libtas.h>
#include <coordonnees.h>

/*
 *   Fonction :    factorielle
 *   
 *   Parametres :  int n, un entier relatif.
 *   
 *   Retour :      int, la factorielle de n.
 *                 
 *   Description : Calcul de la factorielle d'un entier relatif.
 */
int factorielle (int n)
{
  if (n == 0) { return 1; }
  return n * factorielle (n-1);
}

/*
 *   Fonction :    exportationParcours
 *   
 *   Parametres :  TypGraphe* graphe, le graphe contenant les sommets du cycle.
 *                 int* cycle, un cycle du graphe.
 *                 char* fichier_nom, le nom du fichier d'export.
 *                 char* titre, le titre du graphique exporte.
 *   
 *   Retour :      
 *                 
 *   Description : Export d'un script python permettant de visualiser un parcours.
 */
void exportationParcours(TypGraphe* graphe, int* cycle, char* fichier_nom, char* titre)
{
  FILE* fichier = fopen(fichier_nom, "w+");

  if (fichier == NULL) {return;}

  // Entete du script.
  {
    fprintf(fichier, "import matplotlib.pyplot as plt\n");
    fprintf(fichier, "plt.axis([0, 101, 0, 101])\n");
    fprintf(fichier, "plt.grid(True)\n");
    fprintf(fichier, "plt.title('%s')\n", titre);
  }
    
  // Ajout des arêtes.
  fprintf(fichier, "plt.plot([%d", coordonneeX(valeurSommetTypGraphe(graphe, cycle[0])));
  
  for(size_t i=1; i<graphe->nbSommets; i++)
    {
      fprintf(fichier
	      , ", %d"
	      , coordonneeX(valeurSommetTypGraphe(graphe, cycle[i]))
	      );
    }
  
  fprintf(fichier, ", %d], [%d"
	  , coordonneeX(valeurSommetTypGraphe(graphe, cycle[0]))
	  , coordonneeY(valeurSommetTypGraphe(graphe, cycle[0]))
	  );
  
  for(size_t i=1; i<graphe->nbSommets; i++)
    {
      fprintf(fichier
	      , ", %d"
	      , coordonneeY(valeurSommetTypGraphe(graphe, cycle[i]))
	      );
    }

  fprintf(fichier, ", %d], 'b--', label='Cycle de poids %.2f')\n"
	  , coordonneeY(valeurSommetTypGraphe(graphe, cycle[0]))
	  , pvc_cout_cycle(graphe, cycle)
	  );
  
  // Ajout des sommets.
  fprintf(fichier
	  , "plt.plot([%d], [%d], 'ro')\n"
	  , coordonneeX(valeurSommetTypGraphe(graphe, cycle[0]))
	  , coordonneeY(valeurSommetTypGraphe(graphe, cycle[0]))
	  );

  fprintf(fichier, "plt.text(%d, %d, '%d')\n"
	  , coordonneeX(valeurSommetTypGraphe(graphe, cycle[0]))
	  , coordonneeY(valeurSommetTypGraphe(graphe, cycle[0]))
	  , cycle[0]
	  );

  for(size_t i=1; i<graphe->nbSommets; i++)
    {
      fprintf(fichier
  	      , "plt.plot([%d], [%d], 'go')\n"
  	      , coordonneeX(valeurSommetTypGraphe(graphe, i+1))
  	      , coordonneeY(valeurSommetTypGraphe(graphe, i+1))
  	      );

      fprintf(fichier, "plt.text(%d, %d, '%d')\n"
	      , coordonneeX(valeurSommetTypGraphe(graphe, cycle[i]))
	      , coordonneeY(valeurSommetTypGraphe(graphe, cycle[i]))
	      , cycle[i]
	      );
    }

  // Pied du script.
  {
    fprintf(fichier, "plt.legend(loc='upper left')\n");
    fprintf(fichier, "plt.show()\n");
  }
  
  fclose(fichier);
}

/*
 *   Fonction :    pvc_optimal
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou chercher un cycle hamiltonien minimal.
 *                 int* cycle, tableau d'entier representant les sommets dans 
 *                                l'ordre dans le cycle.
 *   
 *   Retour :      double, le poids du cycle trouve.
 *                 
 *   Description : Calcul de la solution optimale du PVC.
 */
double pvc_optimal (TypGraphe* graphe, int* cycle)
{
  int cycleCourant[graphe->nbSommets+1];
  memset(cycleCourant, -1, sizeof(int)*(graphe->nbSommets+1));

  double meilleurCout = ~(1<<(sizeof(double)*4 - 1));
  
  pvc_optimal_partiel(graphe, cycleCourant, 0, 0, cycle, &meilleurCout);  
  
  return meilleurCout;
}

/*
 *   Fonction :    pvc_optimal_partiel
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou chercher un cycle hamiltonien minimal.
 *                 int* cycleCourant, le cycle examine par l'iteration courante.
 *                 int tailleCycle, la longueur du cycle de l'iteration courante.
 *                 double coutCourant, le cout du cycle courant.
 *                 int* meilleurCycle, meilleur cycle retenu dans les iterations precedentes.
 *                 double* meilleurCout, cout du meilleur cycle.
 *   
 *   Retour :      
 *                 
 *   Description : Calcul de la solution optimale du PVC.
 */
void pvc_optimal_partiel (TypGraphe* graphe, int* cycleCourant, int tailleCycle, double coutCourant, int* meilleurCycle, double* meilleurCout)
{
  if(tailleCycle >= graphe->nbSommets)
    {
      cycleCourant[graphe->nbSommets] = cycleCourant[0];
      
      TypVoisins* arete = areteTypGraphe(graphe, cycleCourant[tailleCycle-1], cycleCourant[tailleCycle]);
      
      if (arete != NULL)
	{
	  coutCourant += arete->poids;
	}
      
      if (coutCourant < *meilleurCout)
	{
	  *meilleurCout = coutCourant;
	  memcpy( meilleurCycle, cycleCourant, sizeof(int) * (graphe->nbSommets + 1) );
	}
      
      return;
    }
  
  for(int sommet=1; sommet<=graphe->nbSommets; sommet++)
    {
      int dejaFait = 0;
      
      for(size_t i=0; i<tailleCycle; i++)
	{
	  if (cycleCourant[i] == sommet)
	    {
	      dejaFait = 1;
	      break;
	    }
	}

      if (dejaFait == 1) {continue;}
      
      double coutSuivant = 0.0;
      
      TypVoisins* arete = areteTypGraphe(graphe, cycleCourant[tailleCycle-1], sommet);
      
      if (arete != NULL)
	{
	  coutSuivant = arete->poids;
	}
      
      cycleCourant[tailleCycle] = sommet;
      pvc_optimal_partiel(graphe
			  , cycleCourant
			  , tailleCycle+1
			  , coutCourant + coutSuivant
			  , meilleurCycle
			  , meilleurCout);
    }
}

/*
 *   Fonction :    pvc_plus_proche_voisin
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou chercher un cycle hamiltonien minimal.
 *                 int* cycle, tableau d'entier representant les sommets dans 
 *                                l'ordre dans le cycle.
 *   
 *   Retour :      double, le poids du cycle trouve.
 *                 
 *   Description : Calcul d'une solution approchee au PVC a l'aide de
 *                 la methode du plus proche voisin.
 */
double pvc_plus_proche_voisin (TypGraphe* graphe, int* cycle)
{
  size_t tailleCycle = 0;  
  sommet_t* sommets[graphe->nbSommets];
  double cout = 0.0;
  size_t origine = 1;
  
  for(size_t i=0; i<graphe->nbSommets; i++)
    {
      sommets[i] = (sommet_t*) malloc( sizeof(sommet_t) );
      sommets[i]->numero = i + 1;
      sommets[i]->graphe = graphe;
    }

  // Ajout du premier sommet du cycle.
  size_t sommetCourant = origine - 1;
  
  while (tailleCycle < graphe->nbSommets)
    {
      size_t prochain = graphe->nbSommets;
      double ajoutCout = 0.0;
      
      // Recherche du plus proche voisin.
      {
	prochain = graphe->nbSommets;
	double poidsMin = 1000.0;
	
	for(size_t i=1; i<graphe->nbSommets; i++)
	  {
	    if (i == sommetCourant || sommets[i] == NULL) { continue; }

	    TypVoisins* arete = areteTypGraphe(graphe, sommets[sommetCourant]->numero, i+1);

	    if (arete != NULL && arete->poids < poidsMin)
	      {		
		poidsMin = arete->poids;
		prochain = i;
		ajoutCout = poidsMin;
	      }
	  }	
      }
      
      if(sommetCourant < graphe->nbSommets)
	{
	  cycle[tailleCycle] = sommets[sommetCourant]->numero;
	  free(sommets[sommetCourant]);
	  sommets[sommetCourant] = NULL;
	}

      sommetCourant = prochain;
      cout += ajoutCout;
      tailleCycle++;
    }
  
  cycle[graphe->nbSommets] = origine;
  cout += areteTypGraphe(graphe, cycle[graphe->nbSommets-1], origine)->poids;
  
  return cout;
}

/*
 *   Fonction :    pvc_plus_petit_detour
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou chercher un cycle hamiltonien minimal.
 *                 int* cycle, tableau d'entier representant les sommets dans 
 *                                l'ordre dans le cycle.
 *   
 *   Retour :      double, le poids du cycle trouve.
 *                 
 *   Description : Calcul d'une solution approchee au PVC a l'aide de
 *                 la methode du plus petit detour
 */
double pvc_plus_petit_detour (TypGraphe* graphe, int* cycle)
{
  int sommets[graphe->nbSommets+1];
  for(size_t i=1; i<=graphe->nbSommets+1; i++)
    {
      sommets[i-1] = i;
    }
  
  // Choix d'un petit cycle de trois sommets.
  cycle[0] = 1;  sommets[0] = -1;
  cycle[1] = 2;  sommets[1] = -1;
  cycle[2] = 3;  sommets[2] = -1;
  cycle[3] = 1;

    
  int tailleCycle = 4;
  
  while (tailleCycle < graphe->nbSommets+1)
    {
      double minCout = 5000;
      int minSommet = -1;
      int minPredecesseur = -1;
      
      // Recherche de l'ajout de sommet le moins couteux.
      for(size_t i=0; i<graphe->nbSommets; i++)
	{
	  if (sommets[i] == -1) { continue; }

	  for(size_t pred=0; pred<graphe->nbSommets; pred++)
	    {
	      if (sommets[pred] != -1) {continue;}

	      double coutAvant = pvc_cout_cycle(graphe, cycle);
	      pvc_ajout_sommet_cycle(graphe, cycle, i+1, pred+1);
	      double coutAjout = pvc_cout_cycle(graphe, cycle) - coutAvant;
	      pvc_retrait_sommet_cycle(graphe, cycle, i+1);
	      
	      if (coutAjout < minCout)
		{
		  minPredecesseur = pred;
		  minSommet = i;
		  minCout = coutAjout;
		}
	    }
	}
      
      if(minSommet != -1)
	{
	  sommets[minSommet] = -1;
	  pvc_ajout_sommet_cycle(graphe, cycle, minSommet+1, minPredecesseur + 1);
	  tailleCycle++;
	}
    }

  return pvc_cout_cycle(graphe, cycle);
}

/*
 *   Fonction :    pvc_ajout_sommet_cycle
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou se trouve le cycle.
 *                 int* cycle, tableau d'entier representant les sommets dans 
 *                                l'ordre dans le cycle.
 *                 int sommet, le sommet a ajouter dans le cycle.
 *                 int predecesseur, le sommet precedent le sommet a ajouter au sein du cycle.
 *   
 *   Retour :      
 *                 
 *   Description : Ajout d'un sommet dans un cycle.
 */
void pvc_ajout_sommet_cycle (TypGraphe* graphe, int* cycle, int sommet, int predecesseur)
{
  size_t indicePredecesseur = 0;

  while(cycle[indicePredecesseur] != predecesseur){ indicePredecesseur++; }

  for(size_t i=graphe->nbSommets+1; i>indicePredecesseur+1; i--)
    {
      cycle[i] = cycle[i-1];
    }

  cycle[indicePredecesseur+1] = sommet;
}

/*
 *   Fonction :    pvc_retrait_sommet_cycle
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou se trouve le cycle.
 *                 int* cycle, tableau d'entier representant les sommets dans 
 *                                l'ordre dans le cycle.
 *                 int sommet, le sommet a retirer du cycle.
 *                 int predecesseur, le sommet precedent le sommet a retirer du cycle.
 *   
 *   Retour :      
 *                 
 *   Description : Retrait d'un sommet dans un cycle.
 */
void pvc_retrait_sommet_cycle (TypGraphe* graphe, int* cycle, int sommet)
{
  size_t indiceSommet = 0;
  while(cycle[indiceSommet] != sommet){ indiceSommet++; }

  for(size_t i=indiceSommet; i<graphe->nbSommets; i++)
    {
      cycle[i] = cycle[i+1];
    }

  cycle[graphe->nbSommets] = -1;
  
}

/*
 *   Fonction :    pvc_cout_cycle
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou se trouve le cycle.
 *                 int* cycle, tableau d'entier representant les sommets dans 
 *                                l'ordre dans le cycle.
 *   
 *   Retour :      double, le cout du cycle.
 *                 
 *   Description : Calcul du cout d'un cycle.
 */
double pvc_cout_cycle (TypGraphe* graphe, int* cycle)
{
  double cout = 0.0;
  
  for(size_t i=0; i<graphe->nbSommets; i++)
    {
      if (cycle[i] == -1){continue;}
      
      TypVoisins* arete = areteTypGraphe(graphe, cycle[i], cycle[i+1]);
      
      if (arete != NULL)
	{
	  cout += arete->poids;
	}
    }
  
  return cout;
}

/*
 *   Fonction :    pvc_arpm
 *   
 *   Parametres :  TypGraphe* graphe, le graphe ou chercher un cycle hamiltonien minimal.
 *                 int* cycle, tableau d'entier representant les sommets dans 
 *                                l'ordre dans le cycle.
 *   
 *   Retour :      double, le poids du cycle trouve.
 *                 
 *   Description : Calcul de l'heuristique basee sur l'arpm.
 */
double pvc_arpm (TypGraphe* graphe, int* cycle)
{
  TypGraphe arpm;
  creationArpmTypGraphe(graphe, &arpm);

  size_t tailleCycle = 0;

  int origine = 1;
  
  int vu[graphe->nbSommets];
  int parcouru[graphe->nbSommets];
  memset(vu, 0, sizeof(int)*graphe->nbSommets);
  memset(parcouru, 0, sizeof(int)*graphe->nbSommets);
  
  size_t taillePile = 0;
  int pile[graphe->nbSommets+1];
  
  pile[taillePile] = -1;

  taillePile++;
  pile[taillePile] = origine;
  
  while (pile[taillePile] != -1)
    {
      // Depilement du sommet.
      int sommet = pile[taillePile];
      taillePile--;

      if (parcouru[sommet-1] == 0)
	{
	  cycle[tailleCycle] = sommet;
	  tailleCycle++;
	}
      
      printf("Parcours du sommet %d -> %d.\n", sommet, parcouru[sommet-1]);
      parcouru[sommet-1] = 1;
      
      // Empilement des voisins.
      for(size_t s=0; s<graphe->nbSommets; s++)
	{
	  if(s+1==sommet) { continue; }

	  TypVoisins* iterateur = arpm.listesAdjacences[s];
	  if (iterateur->voisinSuivant == iterateur) { continue; }
	  TypVoisins* sentinelle = iterateur;
	  iterateur = iterateur->voisinSuivant;

	  while(iterateur != sentinelle)
	    {
	      if (iterateur->voisin == sommet && vu[s] == 0)
		{
		  taillePile++;
		  pile[taillePile] = s+1;
		  vu[s] = 1;
		}
	      iterateur = iterateur->voisinSuivant;
	    }
	}
    }

  cycle[tailleCycle] = origine;
  
  affichageTypGraphe(&arpm);
  
  // Suppression
  suppressionTypGraphe(&arpm);
  return 0.0;
}
