// Entetes systeme
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
// Entetes projet
#include <libliste.h>

extern pile_erreur_t* ERREUR;

/*
 *   Fonction :    creationTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, voisin a creer.
 *  			   int valeur, la valeur du voisin a creer.
 *  			   double poids, le poids du voisin a creer.
 *   
 *   Retour :      VOISIN_OK si la creation s'est bien deroulee,
 *                 VOISIN_ERREUR sinon.
 *                 
 *   Description : Creation du TypVoisins et initialisation de ses champs.
 */
erreur_t creationTypVoisins(TypVoisins* voisin, int valeur, double poids)
{
	if(voisin == NULL)
	{
		empilementErreur(ERREUR, "Impossible de creer une liste non initialisee.");
		return VOISIN_ERREUR; 
	}
	
	voisin->voisin          = valeur;
	voisin->poids           = poids;
	voisin->voisinSuivant   = voisin;
	voisin->voisinPrecedent = voisin;
	
    return VOISIN_OK;
}

/*
 *   Fonction :    suppressionTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, le TypVoisins a supprimer.
 *   
 *   Retour :      VOISIN_OK si la suppression s'est bien deroulee,
 *                 VOISIN_ERREUR sinon.
 *                 
 *   Description : Supprime un voisin ainsi que ses voisins. Libere la memoire
 *                 alouee pour le TypVoisin supprime.
 */
erreur_t suppressionTypVoisins(TypVoisins* voisin)
{
	// verification cas d'erreur
	// voisin ne doit pas etre nul
    if (voisin == NULL)
    {
    	empilementErreur(ERREUR, "Impossible de supprimer une liste vide.");
    	return VOISIN_ERREUR;
    }
    
    TypVoisins* dernier = dernierTypVoisins(voisin);
    
    // Cas d'erreur :
    // Impossible d'identifier le dernier voisin de la liste.
    if (dernier == NULL)
    {
    	empilementErreur(ERREUR, "Impossible d'identifier la fin de la liste.");
    	return VOISIN_ERREUR;
    }
    
    while (valeurTypVoisins(dernier) != VALEUR_SENTINELLE)
    {
    	dernier = precedentTypVoisins(dernier);
    	
    	int codeRetour = suppressionQueueTypVoisins(voisin);
    	
    	// Cas d'erreur :
    	// Impossible de supprimer un voisin de la liste.
    	if (codeRetour == VOISIN_ERREUR)
    	{
    		empilementErreur(ERREUR
    		, "Impossible de supprimer cet element de la liste.");
    		                 
    		return VOISIN_ERREUR;
    	}

    }
    
    return VOISIN_OK;
}

/*
 *   Fonction :    valeurTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, voisin dont on veut obtenir la valeur.
 *   
 *   Retour :      La valeur du champ 'voisin' de voisin.
 *   
 *   Description : Permet d'obtenir la valeur d'un voisin. En cas d'erreur,
 *                 un assert est declenche.
 */
int valeurTypVoisins(TypVoisins* voisin)
{
	// verification cas d'erreur
	// voisin ne doit pas etre nul
    assert(voisin);
    
    return voisin->voisin;
}

/*
 *   Fonction :    poidsTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, voisin dont on veut obtenir le poids.
 *   
 *   Retour :      Le poids du champ 'voisin' de voisin.
 *   
 *   Description : Permet d'obtenir le poids d'un voisin. En cas d'erreur,
 *                 un assert est declenche.
 */
double poidsTypVoisins(TypVoisins* voisin)
{
	// verification cas d'erreur
	// voisin ne doit pas etre nul
    assert(voisin);
    
    return voisin->poids;
}

/*
 *   Fonction :    creationQueueTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un voisin appartenant a la liste a 
 *                                     laquelle on souhaite 
 *                                     ajoute un element en queue.
 *                 int valeur, la valeur du voisin a creer.
 *                 double poids, le poids du voisin a creer.
 *
 *   Retour :      Pointeur vers le type voisin insere,
 *                 NULL sinon.
 *                 
 *   Description : Cree un TypVoisins a la fin de la liste a laquelle
 *                 appartient voisin.
 */
TypVoisins* creationQueueTypVoisins(TypVoisins* voisin, int valeur, double poids)
{
	// verification cas d'erreur
	// voisin ne doit pas etre nul
    assert(voisin);

    TypVoisins* nouveauVoisin = (TypVoisins*) malloc( sizeof(TypVoisins) );
    
    int codeCreation  = creationTypVoisins(nouveauVoisin, valeur, poids); 
    int codeInsertion = insertionQueueTypVoisins(voisin, nouveauVoisin);
    
    if (codeCreation == VOISIN_ERREUR
        || codeInsertion == VOISIN_ERREUR)
    {
    	empilementErreur(ERREUR, "Impossible de creer un element en queue de liste.");
    	return NULL; 
    }
    
    return nouveauVoisin;
}

/*
 *   Fonction :    insertionQueueTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un voisin appartenant a la liste a 
 *                                     laquelle on souhaite 
 *                                     ajoute un element en queue.
 *                 TypVoisins* nouveau, le voisin a inserer en queue.
 *                 
 *   Retour :      VOISIN_OK si l'insertion en queue s'est bien deroulee,
 *                 VOISIN_ERREUR sinon.
 *                 
 *   Description : Ajoute un TypVoisins a la fin de la liste a laquelle
 *                 appartient voisin.
 */
erreur_t insertionQueueTypVoisins(TypVoisins* voisin, TypVoisins* nouveau)
{	
	if (voisin == NULL)
	{
		empilementErreur(ERREUR
		, "Impossible d'inserer un element dans une liste non initialisee.");
		
		return VOISIN_ERREUR;
	}
	
	if (nouveau == NULL)
	{
		empilementErreur(ERREUR
		, "Impossible d'inserer un element non initialise dans une liste .");
		return VOISIN_ERREUR;
	}
	
	// Dernier de la liste avant ajout de 'voisin'.
	TypVoisins* dernier = dernierTypVoisins(voisin); 

	// La liste est mal formee : l'absence de sentinelle
	// rend l'identification du dernier element impossible.
	if (dernier == NULL)
	{
		empilementErreur(ERREUR
		, "Impossible d'inserer un element dans une liste ne possedant pas de sentinelle.");
		return VOISIN_ERREUR;
	}
	
	TypVoisins* sentinelle = premierTypVoisins(dernier);

	nouveau->voisinSuivant      = sentinelle;
	nouveau->voisinPrecedent    = dernier;
	sentinelle->voisinPrecedent = nouveau;
	dernier->voisinSuivant      = nouveau;
	
    return VOISIN_OK;
}

/*
 *   Fonction :    suppressionQueueTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un voisin appartenant a la liste a 
 *                                     laquelle on souhaite 
 *                                     supprimer un element en queue.
 *                 
 *   Retour :      VOISIN_OK si la suppression en queue s'est bien deroulee,
 *                 VOISIN_ERREUR sinon.
 *                 
 *   Description : Supprime un TypVoisins a la fin de la liste a laquelle
 *                 appartient voisin et en memoire. On ne peut pas supprimer
 *                 la sentinelle de cette maniere.
 */
erreur_t suppressionQueueTypVoisins(TypVoisins* voisin)
{
	// Voisin ne doit pas etre nul.
	assert(voisin);
	
	TypVoisins* dernier      = dernierTypVoisins(voisin);
	TypVoisins* sentinelle   = premierTypVoisins(voisin);
	TypVoisins* futurDernier = precedentTypVoisins(dernier);
	
	// Cas d'erreur :
	// Impossible de trouver la queue de la liste.
	// Impossible de trouver la sentinelle.
	if (dernier == NULL || sentinelle == NULL)
	{
		empilementErreur(ERREUR
		, "Impossible de supprimer un element en queue dans une liste ne contenant pas de sentinelle.");
		return VOISIN_ERREUR;
	} 
	
	// On ne peut pas supprimer la sentinelle.
	if ( dernier == sentinelle ) { return VOISIN_ERREUR; }
	
	// 1- Suppression et mise-a-jour des liens.
	{
		sentinelle->voisinPrecedent = futurDernier;
		futurDernier->voisinSuivant = sentinelle;
	}
	
	// 2- Suppression des liens de 'dernier'.
	{
		dernier->voisinSuivant      = NULL;
		dernier->voisinPrecedent    = NULL;
	}
	
	// 3- Liberation de 'dernier'.
	{
		free(dernier);
	}
	
    return VOISIN_OK;
}

/*
 *   Fonction :    suppressionVoisinTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un voisin appartenant a la liste dans 
 *                                     laquelle on souhaite le supprimer.
 *                 
 *   Retour :      VOISIN_OK si la suppression en queue s'est bien deroulee,
 *                 VOISIN_ERREUR sinon.
 *                 
 *   Description : Supprime un TypVoisins de la liste a laquelle il
 *                 appartient et en memoire. On ne peut pas supprimer
 *                 la sentinelle de cette maniere.
 */
erreur_t suppressionVoisinTypVoisins(TypVoisins* voisin)
{
	// Le voisin doit exister.
	if (voisin == NULL)
	{ 
		empilementErreur(ERREUR
		, "Impossible de supprimer un element d'une liste non initialisee.");
		return VOISIN_ERREUR; 
	}
	
	// On ne peut pas supprimer la sentinelle.
	if (valeurTypVoisins(voisin) == VALEUR_SENTINELLE) 
	{ 
		empilementErreur(ERREUR, "Impossible de supprimer la sentinelle.");
		return VOISIN_ERREUR; 
	}
	
	// 1- Suppression et mise-a-jour des liens
	precedentTypVoisins(voisin)->voisinSuivant = suivantTypVoisins(voisin);
	suivantTypVoisins(voisin)->voisinPrecedent = precedentTypVoisins(voisin);
	
	// 2- Suppression des liens de 'voisin'.
	voisin->voisinPrecedent = NULL;
	voisin->voisinSuivant = NULL;
	
	// 3- Liberation de 'voisin'.
	free(voisin);
	
	return VOISIN_OK;
}

/*
 *   Fonction :    rechercheValeurTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un TypVoisins quelconque de la liste.
 *                 int valeur, la valeur a rechercher dans la liste.
 *   
 *   Retour :      Un pointeur vers le premier voisin possedant la valeur recherchee.
 *                 En l'absence de resultat, la valeur NULL est retournee.
 *                 
 *   Description : Permet d'obtenir un pointeur vers le voisin premier voisin de 
 *                 la liste de TypVoisins ayant pour valeur une valeur donnee.
 */
TypVoisins* rechercheValeurTypVoisins(TypVoisins* voisin, int valeur)
{
	// Voisin doit exister.
	if (voisin == NULL)
	{ 
		return NULL;
	}	
	
	TypVoisins* sentinelle = premierTypVoisins(voisin);
	TypVoisins* iterateur = sentinelle;
	
	do
	{
		iterateur = suivantTypVoisins(iterateur);
		
		if( valeurTypVoisins(iterateur)== valeur )
		{
			return iterateur;
		}
	}
	while(iterateur != sentinelle);

	return NULL;
}

/*
 *   Fonction :    suivantTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un TypVoisins ayant un voisin suivant.
 *   
 *   Retour :      Un pointeur vers le voisin suivant du parametre 'voisin'. 
 *                 En l'absence de voisin, la valeur NULL est retournee.
 *                 
 *   Description : Permet d'obtenir un pointeur vers le voisin suivant d'un 
 *                 TypVoisins.
 */
TypVoisins* suivantTypVoisins(TypVoisins* voisin)
{
	// Voisin ne doit pas etre nul.
	assert(voisin);
	
    return voisin->voisinSuivant;
}

/*
 *   Fonction :    precedentTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un TypVoisins ayant un voisin precedent.
 *   
 *   Retour :      Un pointeur vers le precedent voisin du parametre 'voisin'. 
 *                 En l'absence de voisin, la valeur NULL est retournee.
 *                 
 *   Description : Permet d'obtenir un pointeur vers le voisin precedent d'un 
 *                 TypVoisins.
 */
TypVoisins* precedentTypVoisins(TypVoisins* voisin)
{
	// Voisin ne doit pas etre nul.
	assert(voisin);
	
    return voisin->voisinPrecedent;
}

/*
 *   Fonction :    premierTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un TypVoisins appartenant a une liste.
 *   
 *   Retour :      Un pointeur vers le premier voisin (sentinelle) de la liste.
 *                 En cas d'erreur, la valeur NULL est retournee.
 *				   Exemple : Absence de sentinelle.
 *                 
 *   Description : Permet d'obtenir un pointeur vers le premier voisin de la 
 *                 liste a laquelle appartient un TypVoisins.
 */
TypVoisins* premierTypVoisins(TypVoisins* voisin)
{
	// Voisin ne doit pas etre nul.
	assert(voisin);

	TypVoisins* iterateur = voisin;
	
	while (valeurTypVoisins(iterateur) != VALEUR_SENTINELLE)
	{

		iterateur = suivantTypVoisins(iterateur);

		// On a parcouru l'ensemble de la liste
		// sans trouver de sentinelle (cas d'erreur)
		if (iterateur == voisin)
		{
			empilementErreur(ERREUR
			, "Impossible de trouver le premier element d'une liste ne contenant pas de sentinelle.");
			return NULL;
		} 
	}

    return iterateur;
}

/*
 *   Fonction :    dernierTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un TypVoisins appartenant a une liste.
 *   
 *   Retour :      Un pointeur vers le dernier voisin de la liste.
 *                 En cas d'erreur, la valeur NULL est retournee.
 *				   Exemple : Absence de sentinelle.
 *                 
 *   Description : Permet d'obtenir un pointeur vers le dernier voisin de la 
 *                 liste a laquelle appartient un TypVoisins.
 */
TypVoisins* dernierTypVoisins(TypVoisins* voisin)
{
	// Voisin ne doit pas etre nul.
	assert(voisin);
	
	TypVoisins* sentinelle = premierTypVoisins(voisin);

	// La liste contenant 'voisin' ne possede pas de sentinelle.
	// On ne peut donc pas identifier le dernier element de la liste.
	if (sentinelle == NULL)
	{
		empilementErreur(ERREUR
			, "Impossible de trouver le dernier element d'une liste ne contenant pas de sentinelle.");
		return NULL;
	}
	
	// Le dernier element de la liste se situe juste avant la sentinelle
	// car la liste est circulaire.
    return precedentTypVoisins(sentinelle);
}

/*
 *   Fonction :    representationTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un TypVoisins quelconque.
 *                 FILE* sortie, la sortie utilisee.
 *   
 *   Retour :      VOISIN_OK si l'affichage est possible.
 *   		       VOISIN_ERREUR si l'element ne peut etre affiche.
 *
 *   Description : Affiche la liste de voisins a laquelle appartient 
 *                 le parametre.
 */
erreur_t representationTypVoisins(TypVoisins* voisin, FILE* sortie)
{
	if (voisin == NULL)
	{
		empilementErreur(ERREUR
			, "Impossible de representer une liste non initialisee.");
			
		return VOISIN_ERREUR;
	}
	
	TypVoisins* sentinelle = premierTypVoisins(voisin);
	
	// Impossible de trouver le premier element de la liste.
	// On ne peut donc pas l'afficher.
	if (sentinelle == NULL)
	{
		empilementErreur(ERREUR
			, "Impossible de representer une liste ne possedant pas de sentinelle.");
		return VOISIN_ERREUR;
	}
	
	TypVoisins* iterateur = suivantTypVoisins(sentinelle);
	
	while (iterateur != sentinelle)
	{
		fprintf( sortie, "(%d/%.2f)", valeurTypVoisins(iterateur) 
		                   	      , poidsTypVoisins(iterateur) );
		  
		                  
		iterateur = suivantTypVoisins(iterateur);
		
		if (iterateur != sentinelle)
		{
			fprintf(sortie, ", ");
		}
	}
	
	return VOISIN_OK;
}

/*
 *   Fonction :    affichageTypVoisins
 *   
 *   Parametres :  TypVoisins* voisin, un TypVoisins quelconque.
 *   
 *   Retour :      VOISIN_OK si l'affichage est possible.
 *   		       VOISIN_ERREUR si l'element ne peut etre affiche.
 *
 *   Description : Affiche la liste de voisins a laquelle appartient 
 *                 le parametre.
 */
erreur_t affichageTypVoisins(TypVoisins* voisin)
{
	erreur_t err = representationTypVoisins(voisin, stdout);
	
	if (err == VOISIN_ERREUR)
	{
		empilementErreur(ERREUR
			, "Impossible d'afficher une liste non representable.");
	}
	
	return err;
}


























